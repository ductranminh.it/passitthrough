LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_SRC_FILES := $(LOCAL_PATH)/hellocpp/main.cpp \
                   $(LOCAL_PATH)/../../../Classes/AppDelegate.cpp \
                   $(LOCAL_PATH)/../../../Classes/AboutScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/adsManager.cpp \
                   $(LOCAL_PATH)/../../../Classes/CloseScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/common.cpp \
                   $(LOCAL_PATH)/../../../Classes/FirstScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/GuideScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/MainGameScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/PixelCollision.cpp \
                   $(LOCAL_PATH)/../../../Classes/RankingScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/RateScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/SettingScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/SplashScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/RemoveAdScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/UnlockAllLevelsScene.cpp \
                   $(LOCAL_PATH)/../../../Classes/SelectLevelScene.cpp

LOCAL_CPPFLAGS := -DSDKBOX_ENABLED
LOCAL_LDLIBS := -landroid \
-llog

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES := PluginAdMob \
sdkbox \
PluginIAP \
PluginShare

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cc_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)
$(call import-add-path,$(LOCAL_PATH))
$(call import-module, cocos)

$(call import-module, ./sdkbox)
$(call import-module, ./pluginadmob)
$(call import-module, ./pluginshare)
$(call import-module, ./pluginiap)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
