/************************************************************************
*  UnlockAllLevelsScene.cpp
*
*
*  Created by udinsoft
*  Copyright udinsoft 2019-5-15
*
************************************************************************/
#include "UnlockAllLevelsScene.h"
#include "FirstScene.h"


#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>

#endif

using namespace cocos2d;
using namespace CocosDenshion;

enum enCloseSceneMenu {
	enOk = 1,
	enClose = 2,
	enMenu
};

Scene* UnlockAllLevelsScene::createScene(){
	return UnlockAllLevelsScene::create();
}

// on "init" you need to initialize your instance
bool UnlockAllLevelsScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}

	initBackground();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(UnlockAllLevelsScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(UnlockAllLevelsScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(UnlockAllLevelsScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	return true;
}


void UnlockAllLevelsScene::initBackground(){
	//background
	setting_background = Sprite::create("pay_screen/unlock-level-broad.png");
	setting_background->setPosition(Vec2(winSize.width *0.5f, winSize.height * 0.5f));
	CommonUtiles::setAdjustScale(setting_background);
	setting_background->setScale(setting_background->getScale()*0.6f);
	addChild(setting_background, 2);
    float m_nWidth = setting_background->getContentSize().width;
    float m_nHeight = setting_background->getContentSize().height;

	//ok menu
	MenuItemImage* btn_ok = MenuItemImage::create("option_screen/ok.png", "option_screen/ok.png", CC_CALLBACK_1(UnlockAllLevelsScene::backCallBack, this));
	btn_ok->setTag(enOk);
	btn_ok->setPosition(Vec2(m_nWidth*0.75f, m_nHeight*0.05f));

	//cancel
	MenuItemImage* btn_cancel = MenuItemImage::create("option_screen/close.png", "option_screen/close.png", CC_CALLBACK_1(UnlockAllLevelsScene::backCallBack, this));
	btn_cancel->setTag(enClose);
	btn_cancel->setPosition(Vec2(m_nWidth*0.25f, m_nHeight*0.05f));

	m_pMainMenu = Menu::create(btn_ok, btn_cancel, NULL);
	m_pMainMenu->setPosition(Vec2::ZERO);
	m_pMainMenu->setTag(enMenu);
	setting_background->addChild(m_pMainMenu, 3);

	touch_status = false;
	instruction_state = false;

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(UnlockAllLevelsScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(UnlockAllLevelsScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(UnlockAllLevelsScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
}

void UnlockAllLevelsScene::backCallBack(Ref* pSender){
	switch (((MenuItem *)pSender)->getTag())
	{

	case enOk:
	{
		this->getParent()->resume();

		menu_halt = false;
		this->removeFromParent();
		Director::getInstance()->resume();

		break;
	}
	case enClose:
	{

		this->getParent()->resume();

		menu_halt = false;
		this->removeFromParent();
		Director::getInstance()->resume();
		//this->getParent()->removeChild(setting_background);

		break;
	}

	}
}

bool UnlockAllLevelsScene::onTouchBegan(Touch* touch, Event* event){
	touchStart = setting_background->convertTouchToNodeSpace(touch);
	return false;
}

void UnlockAllLevelsScene::onTouchMoved(Touch* touch, Event* event){
	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

void UnlockAllLevelsScene::onTouchEnded(Touch* touch, Event* event)
{	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
	
}

