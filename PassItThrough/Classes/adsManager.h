#ifndef __ADSMANAGER_H__
#define __ADSMANAGER_H__

#include "common.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
#define SDKBOX_ENABLED 1
#ifdef SDKBOX_ENABLED
#include "PluginIAP/PluginIAP.h"
#include "PluginAdMob/PluginAdMob.h"
#endif
class AdsManager : public Ref, public sdkbox::IAPListener
#else
class AdsManager : public Ref
#endif
{
public:
	virtual bool init();
    AdsManager();
    ~AdsManager();
	//CREATE_FUNC(AdsManager);
    
	static AdsManager* getInstance();

	void moreGames();
	void showAddMob();
	void showPlayhaven();
	void showChartBoost();
	void showFBConnection(int level,int score);
	void showFullAdmob();
	void showAdMob();
	void hideAdMob();
	void postTW(int level,int score);

	void showRate();
	void OpenURL(const char* url);
	void buyItem(std::string str);
	void restoreItem();

private:
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)

	// a selector callback

	void onRequestIAP(cocos2d::Ref* sender);
	void onRestoreIAP(cocos2d::Ref* sender);
	void onIAP(cocos2d::Ref* sender);

	void updateIAP(const std::vector<sdkbox::Product>& products);
	virtual void onInitialized(bool ok) override;
	virtual void onSuccess(sdkbox::Product const& p) override;
	virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
	virtual void onCanceled(sdkbox::Product const& p) override;
	virtual void onRestored(sdkbox::Product const& p) override;
	virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
	virtual void onProductRequestFailure(const std::string &msg) override;
	void onRestoreComplete(bool ok, const std::string &msg) override;

	std::vector<sdkbox::Product> _products;
#endif
};
#endif
