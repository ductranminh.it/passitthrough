/************************************************************************
*  FirstScene.cpp														
*																		
*																		
*  Created by udinsoft									
*  Copyright udinsoft 2019-5-15											
*																		
************************************************************************/

#include "FirstScene.h"
#include "SelectLevelScene.h"
#include "RemoveAdScene.h"
#include "MainGameScene.h"
#include "SettingScene.h"
#include "CloseScene.h"
#include "AboutScene.h"
#include "RankingScene.h"
#include "RateScene.h"
#include "adsManager.h"
#include "common.h"


using namespace cocos2d;
using namespace CocosDenshion;

enum enFirstSceneMenu{
	enFirstSceneMenu_playgame			= 1,
	enFirstSceneMenu_Scoreboard			= 2,
	enFirstSceneMenu_Setting			= 3,
	enFirstSceneMenu_About				= 4,
	enFirstSceneMenu_Exit               = 5,
	enFirstSceneMenu_Facebook			= 6,
	enFirstSceneMenu_Twitter			= 7,
	enFirstSceneMenu_Google				= 8,
	enFirstSceneMenu_IAP                = 9,
	enShareOn							= 10,
	enFirstSceneMenu_Remove_Ad          = 11,
    enFirstSceneMenu_Restore
};


FirstScene* FirstScene::create(){
	auto scene = new FirstScene;
	scene->init();
	scene->autorelease();
	return scene;
}

Scene* FirstScene::createScene(){
	return FirstScene::create();
}

FirstScene::FirstScene(){
	SimpleAudioEngine::getInstance()->preloadEffect(MP3_BUTTON_HIT);
}

FirstScene::~FirstScene(){
	SimpleAudioEngine::getInstance()->unloadEffect(MP3_BUTTON_HIT);
}

// on "init" you need to initialize your instance
bool FirstScene::init(){

	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}

	music_state = UserDefault::getInstance()->getBoolForKey("music_state", true);
	sound_state = UserDefault::getInstance()->getBoolForKey("sound_state", true);
	
	// background
	m_pBackground = Sprite::create("common/bg.png");

	m_pBackground->setScaleX(winSize.width / m_pBackground->getContentSize().width * fScaleRetina);
	m_pBackground->setScaleY(winSize.height / m_pBackground->getContentSize().height * fScaleRetina);
	m_pBackground->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));

	addChild(m_pBackground, 0);


	// play button
	Sprite* pSprite1 = Sprite::create("button/play_firstscene.png");
	Sprite* pSprite2 = Sprite::create("button/play_firstscene.png");
	pSprite2->setOpacity(150);
	m_pPlayItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pPlayItem);
	m_pPlayItem->setScale(m_pPlayItem->getScale() * 0.4f);
	m_pPlayItem->setPosition(Vec2(winSize.width * 0.5f, -winSize.height * 0.3f));
	m_pPlayItem->setTag(enFirstSceneMenu_playgame);

	// scoreboard button
	pSprite1 = Sprite::create("button/ranking_firstscene.png");
	pSprite2 = Sprite::create("button/ranking_firstscene.png");
	pSprite2->setOpacity(150);
	m_pScoreBoardItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pScoreBoardItem);
	m_pScoreBoardItem->setScale(m_pScoreBoardItem->getScale() * 0.4f);
	m_pScoreBoardItem->setPosition(Vec2(-winSize.width * 0.3f, winSize.height * 0.25f));
	m_pScoreBoardItem->setTag(enFirstSceneMenu_Scoreboard);


	// exit button
	pSprite1 = Sprite::create("button/exit_firstscene.png");
	pSprite2 = Sprite::create("button/exit_firstscene.png");
	pSprite2->setOpacity(150);
	m_pExitItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pExitItem);
	m_pExitItem->setScale(m_pExitItem->getScale() * 0.4f);
	m_pExitItem->setPosition(Vec2(winSize.width * 1.3f, winSize.height * 0.25f));
	m_pExitItem->setTag(enFirstSceneMenu_Exit);

	// setting button
	pSprite1 = Sprite::create("button/option_firstscene.png");
	pSprite2 = Sprite::create("button/option_firstscene.png");
	pSprite2->setOpacity(150);
	m_pSettingItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pSettingItem);
	m_pSettingItem->setScale(m_pSettingItem->getScale() * 0.4f);
	m_pSettingItem->setPosition(Vec2(winSize.width * 1.15f, winSize.height * 0.35f));
	m_pSettingItem->setTag(enFirstSceneMenu_Setting);
    
 #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    //restore
    pSprite1 = Sprite::create("button/restore.png");
    pSprite2 = Sprite::create("button/restore.png");
    pSprite2->setOpacity(150);
    m_pRestoreItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
    CommonUtiles::setAdjustScale(m_pRestoreItem);
    m_pRestoreItem->setScale(m_pRestoreItem->getScale() * 0.4f);
    m_pRestoreItem->setPosition(Vec2(winSize.width * 1.15f, winSize.height * 0.35f));
    m_pRestoreItem->setTag(enFirstSceneMenu_Restore);
#endif
    

	// about button
	pSprite1 = Sprite::create("button/about_firstscene.png");
	pSprite2 = Sprite::create("button/about_firstscene.png");
	pSprite2->setOpacity(150);
	m_pAboutItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pAboutItem);
	m_pAboutItem->setScale(m_pAboutItem->getScale() * 0.4f);
	m_pAboutItem->setPosition(Vec2(-winSize.width * 0.15f, winSize.height * 0.35f));
	m_pAboutItem->setTag(enFirstSceneMenu_About);

	//remove ad button

	pSprite1 = Sprite::create("button/remove_ad_firstscene.png");
	pSprite2 = Sprite::create("button/remove_ad_firstscene.png");
	pSprite2->setOpacity(150);
	m_pRemoveAdItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pRemoveAdItem);
	m_pRemoveAdItem->setScale(m_pRemoveAdItem->getScale() * 0.4f);
	m_pRemoveAdItem->setPosition(Vec2(-winSize.width * 0.15f, winSize.height * 0.25f));
	m_pRemoveAdItem->setTag(enFirstSceneMenu_Remove_Ad);
    
    // facebook button
    pSprite1 = Sprite::create("button/facebook.png");
    pSprite2 = Sprite::create("button/facebook.png");
    pSprite2->setOpacity(150);
    m_pFacebookItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
    CommonUtiles::setAdjustScale(m_pFacebookItem);
    m_pFacebookItem->setScale(m_pFacebookItem->getScale() * 0.4f);
    m_pFacebookItem->setPosition(Vec2(winSize.width * 0.28f, winSize.height * 0.08f));
    m_pFacebookItem->setTag(enFirstSceneMenu_Facebook);
    
    // twitter button
    pSprite1 = Sprite::create("button/twitter.png");
    pSprite2 = Sprite::create("button/twitter.png");
    pSprite2->setOpacity(150);
    m_pTwitterItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
    CommonUtiles::setAdjustScale(m_pTwitterItem);
    m_pTwitterItem->setScale(m_pTwitterItem->getScale() * 0.4f);
    m_pTwitterItem->setPosition(Vec2(winSize.width * 0.37f, winSize.height * 0.08f));
    m_pTwitterItem->setTag(enFirstSceneMenu_Twitter);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	if(is_iap_enabled){
	    if(remove_ad)
        {
            AdsManager::getInstance()->hideAdMob();
            m_pRemoveAdItem->setVisible(false);
        }
	}else
	{ 
		AdsManager::getInstance()->hideAdMob();
		m_pRemoveAdItem->setVisible(false);
	}
#else
    AdsManager::getInstance()->hideAdMob();
    m_pRemoveAdItem->setVisible(false);
    m_pTwitterItem->setVisible(false);
    m_pFacebookItem->setVisible(false);
#endif

	

	// google button
	pSprite1 = Sprite::create("button/google.png");
	pSprite2 = Sprite::create("button/google.png");
	pSprite2->setOpacity(150);
	m_pGoogleItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pGoogleItem);
	m_pGoogleItem->setScale(m_pGoogleItem->getScale() * 0.4f);
	m_pGoogleItem->setPosition(Vec2(winSize.width * 0.64f, winSize.height * 0.08f));
	m_pGoogleItem->setTag(enFirstSceneMenu_Google);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	// rate button-
	pSprite1 = Sprite::create("button/store.png");
	pSprite2 = Sprite::create("button/store.png");
	pSprite2->setOpacity(150);
	m_pIAPItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(FirstScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pIAPItem);
	m_pIAPItem->setScale(m_pIAPItem->getScale() * 0.4f);
	m_pIAPItem->setPosition(Vec2(winSize.width * 0.87f, winSize.height * 0.08f));
	m_pIAPItem->setTag(enFirstSceneMenu_IAP);
    pMenu = Menu::create(m_pPlayItem, m_pScoreBoardItem, m_pSettingItem, m_pAboutItem, m_pExitItem, m_pFacebookItem, m_pTwitterItem, m_pIAPItem, m_pRemoveAdItem, NULL);
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    pMenu = Menu::create(m_pPlayItem, m_pScoreBoardItem, m_pSettingItem, m_pAboutItem, m_pExitItem, m_pFacebookItem, m_pTwitterItem, m_pRemoveAdItem,m_pRestoreItem, NULL);
#else
    pMenu = Menu::create(m_pPlayItem, m_pScoreBoardItem, m_pSettingItem, m_pAboutItem, m_pExitItem, m_pFacebookItem, m_pTwitterItem, m_pRemoveAdItem, NULL);
#endif
	
	pMenu->setPosition(Vec2(0.f, 0.f));
	addChild(pMenu, 2);
	pMenu->setVisible(false);

	// share on
	Sprite* m_pShareOn = Sprite::create("button/share-on.png");
	CommonUtiles::setAdjustScale(m_pShareOn);
	m_pShareOn->setPosition(Vec2(winSize.width * 0.15f, winSize.height * 0.08f));
	m_pShareOn->setScale(m_pShareOn->getScale()*0.5f);
	m_pShareOn->setTag(enShareOn);
	addChild(m_pShareOn, 2);
    
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	Sprite* m_pRateUs = Sprite::create("button/rate-us.png");
	CommonUtiles::setAdjustScale(m_pRateUs);
	m_pRateUs->setPosition(Vec2(winSize.width * 0.75f, winSize.height * 0.08f));
	m_pRateUs->setScale(m_pRateUs->getScale()*0.5f);
	m_pRateUs->setTag(enShareOn);
	addChild(m_pRateUs, 2);
#endif

	schedule(schedule_selector(FirstScene::OnTimerLoading), 0.5f);

	level_or_chapter = false;     /////if true,level stage shows level select menu in selectLevelScene,and false then chapter select menu in selectLevelScene
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    sdkbox::PluginShare::setListener(this);
	sdkbox::PluginShare::init();
	//sdkbox::PluginShare::setFileProviderAuthorities("com.magez.passitthrough.fileprovider");
#endif

//Back quit app
    auto touchListener = EventListenerKeyboard::create();
    touchListener->onKeyReleased = [this](cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
    {
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK)
        {
            menu_halt=true;
            Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
            pMenu->setEnabled(false);
            unscheduleAllCallbacks();
            //pauseSchedulerAndActions();

            auto layer=CloseScene::create();
            layer->setPosition(Vec2::ZERO);
            addChild(layer,4);

            schedule(schedule_selector(FirstScene::onTimer),0.1f);
        }
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

    auto _listener1 = EventListenerCustom::create(EVENT_BUY_SUCCESS, std::bind(&FirstScene::execute, this, std::placeholders::_1));
    if(_eventDispatcher->hasEventListener(EVENT_BUY_SUCCESS)){
        _eventDispatcher->removeCustomEventListeners(EVENT_BUY_SUCCESS);
    }
    _eventDispatcher->addEventListenerWithFixedPriority(_listener1, 1);

    auto _listener2 = EventListenerCustom::create(EVENT_RT_SUCCESS, std::bind(&FirstScene::execute, this, std::placeholders::_1));
    if(_eventDispatcher->hasEventListener(EVENT_RT_SUCCESS)){
        _eventDispatcher->removeCustomEventListeners(EVENT_RT_SUCCESS);
    }
    _eventDispatcher->addEventListenerWithFixedPriority(_listener2, 1);



	return true;
}

void FirstScene::execute(EventCustom* event){

    std::string nameEvent = event->getEventName().c_str();
    if (nameEvent == EVENT_BUY_SUCCESS || nameEvent == EVENT_RT_SUCCESS) {
        m_pRemoveAdItem->setVisible(false);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
        if (nameEvent == EVENT_RT_SUCCESS) {
            m_pRestoreItem->setVisible(false);
            MessageBox("Your items are restored successfully", "Congratulations");
        }
#endif
        
    }
}

void FirstScene::keyBackClicked()
{
	////CCLog("back button clicked");
	menu_halt=true;
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
	pMenu->setEnabled(false);
	//unscheduleAllCallbacks();
	unscheduleAllCallbacks();

	auto layer=CloseScene::createScene();
	layer->setPosition(Vec2::ZERO);
	addChild(layer,4);

	schedule(schedule_selector(FirstScene::onTimer),0.1f);
}

void FirstScene::InitBackground(float dt){
	Sprite* temp1;
	Sprite* temp2;
	Sprite* temp3;
	Sprite* temp4;
	auto winSize = Director::getInstance()->getWinSize();
	int nWidth = winSize.width;
	int nHeight = winSize.height;

	if(nWidth % 1024 == 0 && nHeight % 768 == 0){
		temp1 = Sprite::create("common/bg_1024_768_1.png");
		temp2 = Sprite::create("common/bg_1024_768_2.png");
		temp3 = Sprite::create("common/bg_1024_768_1.png");
		temp4 = Sprite::create("common/bg_1024_768_2.png");

	}
	else if(nWidth % 1280 == 0 && nHeight % 800 == 0){
		temp1 = Sprite::create("common/bg_1280_800_1.png");
		temp2 = Sprite::create("common/bg_1280_800_2.png");
		temp3 = Sprite::create("common/bg_1280_800_1.png");
		temp4 = Sprite::create("common/bg_1280_800_2.png");
	}
	else if(nWidth % 800 == 0 && nHeight % 480 == 0){
		temp1 = Sprite::create("common/bg_800_480_1.png");
		temp2 = Sprite::create("common/bg_800_480_2.png");
		temp3 = Sprite::create("common/bg_800_480_1.png");
		temp4 = Sprite::create("common/bg_800_480_2.png");
	}
	else if(nWidth % 480 == 0 && nHeight % 320 == 0){
		temp1 = Sprite::create("common/bg_960_640_1.png");
		temp2 = Sprite::create("common/bg_960_640_2.png");
		temp3 = Sprite::create("common/bg_960_640_1.png");
		temp4 = Sprite::create("common/bg_960_640_2.png");
	}
	else if(nWidth % 568 == 0 && nHeight % 320 == 0){
		temp1 = Sprite::create("common/bg_1136_640_1.png");
		temp2 = Sprite::create("common/bg_1136_640_2.png");
		temp3 = Sprite::create("common/bg_1136_640_1.png");
		temp4 = Sprite::create("common/bg_1136_640_2.png");
	}
	else{
		temp1 = Sprite::create("common/bg_800_480_1.png");
		temp2 = Sprite::create("common/bg_800_480_2.png");
		temp3 = Sprite::create("common/bg_800_480_1.png");
		temp4 = Sprite::create("common/bg_800_480_2.png");
	}
	Animation *ani_run = Animation::create();
	SpriteFrame * frame1 = temp1->getSpriteFrame();
	temp1->setScaleX(winSize.width / temp1->getContentSize().width * fScaleRetina);
	temp1->setScaleY(winSize.height / temp1->getContentSize().height * fScaleRetina);
	ani_run->addSpriteFrame(frame1);
	SpriteFrame * frame2= temp2->getSpriteFrame();
	temp2->setScaleX(winSize.width / temp2->getContentSize().width * fScaleRetina);
	temp2->setScaleY(winSize.height / temp2->getContentSize().height * fScaleRetina);
	ani_run->addSpriteFrame(frame2);
	SpriteFrame * frame3 = temp3->getSpriteFrame();
	temp3->setScaleX(winSize.width / temp3->getContentSize().width * fScaleRetina);
	temp3->setScaleY(winSize.height / temp3->getContentSize().height * fScaleRetina);
	ani_run->addSpriteFrame(frame3);
	SpriteFrame * frame4 = temp4->getSpriteFrame();
	temp4->setScaleX(winSize.width / temp4->getContentSize().width * fScaleRetina);
	temp4->setScaleY(winSize.height / temp4->getContentSize().height * fScaleRetina);
	ani_run->addSpriteFrame(frame4);
	ani_run->setDelayPerUnit(0.1f);
	ani_run->setRestoreOriginalFrame(true);
	Sprite* back_image=Sprite::create();
	Animate*  action = Animate::create(ani_run);
	back_image->runAction(action);
	back_image->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	addChild(back_image,1);
}

void FirstScene::OnTimerLoading(float dt){

	unschedule( schedule_selector( FirstScene::OnTimerLoading));
	showMainScreen();
}

void FirstScene::showMainScreen(){
    AdsManager::getInstance()->hideAdMob();      ////admob hide
	showMainMenu();
}

void FirstScene::showMainMenu(){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    if(first_install)AdsManager::getInstance()->restoreItem();
#endif

	auto winSize = Director::getInstance()->getWinSize();
#if (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	getChildByTag(enShareOn)->setVisible(true);
#else
    getChildByTag(enShareOn)->setVisible(false);
#endif
	pMenu->setVisible(true);

	MoveBy* move1=MoveBy::create(0.5f,Vec2(0,winSize.height*0.6f));
	MoveBy* move2=MoveBy::create(0.5f,Vec2(winSize.width*0.6f,0));
	MoveBy* move3=MoveBy::create(0.5f,Vec2(-winSize.width*0.3f,0));
	MoveBy* move4=MoveBy::create(0.5f,Vec2(winSize.width*0.3f,0));
	MoveBy* move5=MoveBy::create(0.5f,Vec2(-winSize.width*0.6f,0));
	MoveBy* move6=MoveBy::create(0.5f,Vec2(winSize.width*0.3f, winSize.height*0.3f));
	m_pPlayItem->runAction(EaseBackOut::create(move1));
	m_pScoreBoardItem->runAction(EaseBackOut::create(move2));
	m_pSettingItem->runAction(EaseBackOut::create(move3));
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    MoveBy* move7=MoveBy::create(0.5f,Vec2(-winSize.width*0.3f,winSize.height*0.3f));
    m_pRestoreItem->runAction(EaseBackOut::create(move7));
    if (remove_ad && unlock_all) {
        m_pRestoreItem->setVisible(false);
    }
#endif
	m_pAboutItem->runAction(EaseBackOut::create(move4));
	m_pExitItem->runAction(EaseBackOut::create(move5));
	m_pRemoveAdItem->runAction(EaseBackOut::create(move6));

	//show ads
#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    ad_booting = false;
    first_booting = false;
#endif
    if(ad_booting && !remove_ad && is_iap_enabled){
        showRemoveAdScene();
        ad_booting=false;
    }
	if(first_booting && !remove_ad && is_iap_enabled){

	    AdsManager::getInstance()->showFullAdmob();
        first_booting=false;
	}
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	//show rate
	if (rate_booting && !nothanks_flag) {
		rate_booting = false;
		showRateScene();
	}
#endif

}

void FirstScene::onTimer(float dt){
	if(!menu_halt){
		pMenu->setEnabled(true);
		Director::getInstance()->getEventDispatcher()->resumeEventListenersForTarget(this);
		unschedule(schedule_selector(FirstScene::onTimer));
	}
}

void FirstScene::backCallBack(Ref* pSender){
	
	playBtnClkSound();
	switch(((MenuItem *)pSender)->getTag())
	{
		
	case enFirstSceneMenu_playgame:
		{
			
			Director *pDirector = Director::getInstance();
			auto pScene = SelectLevelScene::createScene();
			pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
			break;
		}

	case enFirstSceneMenu_Scoreboard:
		{
			
			menu_halt=true;
			Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
			pMenu->setEnabled(false);
			unscheduleAllCallbacks();

			auto layer=RankingScene::createScene();
			layer->setPosition(Vec2::ZERO);
			addChild(layer,4);

			schedule(schedule_selector(FirstScene::onTimer),0.1f);
			break;
		}

	case enFirstSceneMenu_Setting:
		{
			menu_halt=true;
			Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
			pMenu->setEnabled(false);
			unscheduleAllCallbacks();

			auto layer=SettingScene::createScene();
			layer->setPosition(Vec2::ZERO);
			addChild(layer,4);

			schedule(schedule_selector(FirstScene::onTimer),0.1f);

			break;
		}
	case enFirstSceneMenu_Exit:
		{
			menu_halt=true;
			Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
			pMenu->setEnabled(false);
			unscheduleAllCallbacks();
			//pauseSchedulerAndActions();

			auto layer=CloseScene::create();
			layer->setPosition(Vec2::ZERO);
			addChild(layer,4);

			schedule(schedule_selector(FirstScene::onTimer),0.1f);

			break;
		}
	case enFirstSceneMenu_About:
		{
			menu_halt=true;
			Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
			pMenu->setEnabled(false);
			unscheduleAllCallbacks();
			//pauseSchedulerAndActions();

			auto layer=AboutScene::createScene();
			layer->setPosition(Vec2::ZERO);
			addChild(layer,4);

			schedule(schedule_selector(FirstScene::onTimer),0.1f);

			break;

		}
	case enFirstSceneMenu_IAP:
		{
            AdsManager::getInstance()->showRate();

			break;
		}
		
	case enFirstSceneMenu_Facebook:
		{
            const char* url = str_concat(FACEBOOK_SHARE_URL, STORE_URL);
			AdsManager::getInstance()->OpenURL(url);
			break;
		}
	case enFirstSceneMenu_Twitter:
		{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
            sdkbox::SocialShareInfo info;
            info.text = "Can you pass it through? Download here";
            info.title = "Pass It Through";
            info.image = "...";
            info.link = STORE_URL;
            info.platform = sdkbox::SocialPlatform::Platform_Twitter;
            info.showDialog = true;
            sdkbox::PluginShare::share(info);
#endif
			break;

		}
	case enFirstSceneMenu_Google:
		{
			AdsManager::getInstance()->OpenURL("");
			break;
		}
	case enFirstSceneMenu_Remove_Ad:
		{
			//removeAd();
			AdsManager::getInstance()->buyItem(REMOVE_ADS);
			break;
		}
            
    case enFirstSceneMenu_Restore:
        {
            AdsManager::getInstance()->restoreItem();
            break;
        }
	}
	
}

void FirstScene::showRemoveAdScene() {
	menu_halt = true;
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
	pMenu->setEnabled(false);
	unscheduleAllCallbacks();

	auto layer = RemoveAdScene::createScene();
	layer->setPosition(Vec2::ZERO);
	addChild(layer, 4);

	schedule(schedule_selector(FirstScene::onTimer), 0.1f);
}

void FirstScene::showRateScene(){
	menu_halt=true;
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
	pMenu->setEnabled(false);
	unscheduleAllCallbacks();
	//pauseSchedulerAndActions();

	auto layer=RateScene::createScene();
	layer->setPosition(Vec2::ZERO);
	addChild(layer,4);

	schedule(schedule_selector(FirstScene::onTimer),0.1f);
}


void FirstScene::playBtnClkSound(){
	if(sound_state){
	    //CCLOG("#FS-------------------------- : %f",Math.round(sound_volume));
		SimpleAudioEngine::getInstance()->setEffectsVolume(sound_volume < 0.2 ? 0 : sound_volume );
		SimpleAudioEngine::getInstance()->playEffect(MP3_BUTTON_HIT);

	}
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)

void FirstScene::onShareState(const sdkbox::SocialShareResponse& response)
{
    switch (response.state) {
        case sdkbox::SocialShareState::SocialShareStateNone: {
            CCLOG("SharePlugin::onShareState none");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateUnkonw: {
            CCLOG("SharePlugin::onShareState unkonw");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateBegin: {
            CCLOG("SharePlugin::onShareState begin");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateSuccess: {
            CCLOG("SharePlugin::onShareState success");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateFail: {
            CCLOG("SharePlugin::onShareState fail, error:%s", response.error.c_str());
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateCancelled: {
            CCLOG("SharePlugin::onShareState cancelled");
            break;
        }
        case sdkbox::SocialShareStateSelectShow: {
            CCLOG("SharePlugin::onShareState show pancel %d", response.platform);
            break;
        }
        case sdkbox::SocialShareStateSelectCancelled: {
            CCLOG("SharePlugin::onShareState show pancel cancelled %d", response.platform);
            break;
        }
        case sdkbox::SocialShareStateSelected: {
            CCLOG("SharePlugin::onShareState show pancel selected %d", response.platform);
            break;
        }
        default: {
            CCLOG("SharePlugin::onShareState");
            break;
        }
    }
}

#endif

