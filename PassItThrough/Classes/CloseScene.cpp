#include "CloseScene.h"
//#include "SelectLevelScene.h"

#include "MainGameScene.h"
//#include "GameContinueScene.h"
#include "FirstScene.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>

#endif

using namespace cocos2d;
using namespace CocosDenshion;

enum enCloseSceneMenu{
	enOk				=1,
	enClose             =2,
	enMenu
};

Scene* CloseScene::createScene(){
	return CloseScene::create();
}

// on "init" you need to initialize your instance
bool CloseScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}

	initBackground();
	return true;
}

void CloseScene::initBackground(){
	//background
	setting_background = Sprite::create("exit_game_screen/exit-broad.png");
 	setting_background->setPosition( Vec2(winSize.width *0.5f, winSize.height * 0.5f));
	CommonUtiles::setAdjustScale(setting_background);
	setting_background->setScale(setting_background->getScale()*0.6f);
	addChild(setting_background,2);
	float m_nWidth=setting_background->getContentSize().width;
	float m_nHeight=setting_background->getContentSize().height;

	//ok menu
	MenuItemImage* btn_ok = MenuItemImage::create("option_screen/ok.png", "option_screen/ok.png", CC_CALLBACK_1(CloseScene::backCallBack, this));
	btn_ok->setTag(enOk);
	btn_ok->setPosition(Vec2(m_nWidth*0.75f, m_nHeight*0.05f));

	//cancel
	MenuItemImage* btn_cancel = MenuItemImage::create("option_screen/close.png", "option_screen/close.png", CC_CALLBACK_1(CloseScene::backCallBack, this));
	btn_cancel->setTag(enClose);
	btn_cancel->setPosition(Vec2(m_nWidth*0.25f, m_nHeight*0.05f));
	
	m_pMainMenu=Menu::create(btn_ok,btn_cancel,NULL);	
	m_pMainMenu->setPosition(Vec2::ZERO);
	m_pMainMenu->setTag(enMenu);
	setting_background->addChild(m_pMainMenu,3);

	touch_status=false;
	instruction_state=false;

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(CloseScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(CloseScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(CloseScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
}

void CloseScene::backCallBack(Ref* pSender){
	switch(((MenuItem *)pSender)->getTag())
	{
	
	case enOk:			
		{
			Director::getInstance()->end();
#if ( CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
			exit(0);
#endif
			break;
		}
	case enClose:			
		{
			
			this->getParent()->resume();

			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();
			//this->getParent()->removeChild(setting_background);

			break;
		}

	}
}

bool CloseScene::onTouchBegan(Touch* touch, Event* event){
	touchStart = setting_background->convertTouchToNodeSpace(touch);
	return false;
}

void CloseScene::onTouchMoved(Touch* touch, Event* event){
	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

void CloseScene::onTouchEnded(Touch* touch, Event* event){
	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

