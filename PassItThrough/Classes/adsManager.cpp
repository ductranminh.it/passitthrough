#include "adsManager.h"
#include "common.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <jni.h>
#include "jni/JniHelper.h"
#include <android/log.h>
#endif
//////////game center start

static std::string kHomeBanner = "home";
static std::string kGameOverAd = "gameover";
static std::string kRewardedAd = "rewarded";

AdsManager* adsManager = NULL;

AdsManager::AdsManager(){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    sdkbox::PluginAdMob::init();
    sdkbox::IAP::setListener(this);
    sdkbox::IAP::init();
    sdkbox::IAP::setDebug(false);

#endif
}
AdsManager::~AdsManager(){

}

AdsManager* AdsManager::getInstance()
{
	if(adsManager == NULL)
	{
		adsManager = new AdsManager();
		if(!adsManager->init())
		{
			delete adsManager;
			adsManager = NULL;
			return NULL;
		}
		adsManager->retain();
	}
	return adsManager;
}

bool AdsManager::init()
{
	bool bRet = false;
	do
	{

		//init here
		bRet = true;
	}while(0);

	return bRet;
}

void AdsManager::moreGames()
{
	//CCLog("Show More Games");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo t;

	if (JniHelper::getStaticMethodInfo(t, "org/cocos2dx/lib/Cocos2dxHelper", "showChartBoostMoreApp", "()V")) {

		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
#endif
}

void AdsManager::buyItem(std::string str){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	sdkbox::IAP::purchase(str);
#endif
}

void AdsManager::showAddMob()
{
	//CCLog("AddMob Show");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo t;

	if (JniHelper::getStaticMethodInfo(t, "org/cocos2dx/lib/Cocos2dxHelper", "showAddMobView", "()V")) {

		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
#endif
}

void AdsManager::showPlayhaven()
{
	//CCLog("show rate");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo t;

	if (JniHelper::getStaticMethodInfo(t, "org/cocos2dx/lib/Cocos2dxHelper", "showPlayHaven", "()V")) {

		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
#endif
}

void AdsManager::restoreItem(){

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	sdkbox::IAP::restore();
#endif
}

void AdsManager::showChartBoost()
{
	//CCLog("show rate");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo t;

	if (JniHelper::getStaticMethodInfo(t, "org/cocos2dx/lib/Cocos2dxHelper", "showChartBoost", "()V")) {

		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
#endif
}
void AdsManager::showFBConnection(int level,int score)
{
	//CCLog("show Facebook Connection");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo t;

	if (JniHelper::getStaticMethodInfo(t, "org/cocos2dx/lib/Cocos2dxHelper", "postFB", "(II)V")) {

		t.env->CallStaticVoidMethod(t.classID, t.methodID,level,score);
		t.env->DeleteLocalRef(t.classID);
	}
#endif
}
void AdsManager::showFullAdmob()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	if(sdkbox::PluginAdMob::isAvailable(kHomeBanner)) sdkbox::PluginAdMob::show(kGameOverAd);
#endif
}

void AdsManager::showAdMob()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	if(sdkbox::PluginAdMob::isAvailable(kHomeBanner)) sdkbox::PluginAdMob::show(kHomeBanner);
#endif
}

void AdsManager::hideAdMob()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	if(sdkbox::PluginAdMob::isAvailable(kHomeBanner)) sdkbox::PluginAdMob::hide(kHomeBanner);
#endif
}

void AdsManager::showRate()
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	Application::getInstance()->openURL("");
#else
	Application::getInstance()->openURL("https://play.google.com/store/apps/details?id=com.magez.passitthrough");
#endif
}


void AdsManager::OpenURL(const char* url)
{
	Application::getInstance()->openURL(url);
/*#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo t;

	if (JniHelper::getStaticMethodInfo(t, "org/cocos2dx/lib/Cocos2dxHelper", "OpenURL", "(Ljava/lang/String;)V")) {

		jstring StringArg1 = t.env->NewStringUTF(url);
		t.env->CallStaticVoidMethod(t.classID, t.methodID,StringArg1);
		t.env->DeleteLocalRef(t.classID);
	}
#endif*/
}

void AdsManager::postTW(int level,int score)
{
	//CCLog("show Twitter Connection");
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniMethodInfo t;

	if (JniHelper::getStaticMethodInfo(t, "org/cocos2dx/lib/Cocos2dxHelper", "postTW", "(II)V")) {

		t.env->CallStaticVoidMethod(t.classID, t.methodID,level,score);
		t.env->DeleteLocalRef(t.classID);
	}
#endif
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)

//------------------------------------------------------------------------------------
void AdsManager::onRequestIAP(cocos2d::Ref* sender)
{
	sdkbox::IAP::refresh();
}

//------------------------------------------------------------------------------------
void AdsManager::onRestoreIAP(cocos2d::Ref* sender)
{
	sdkbox::IAP::restore();
}

//------------------------------------------------------------------------------------
void AdsManager::onIAP(cocos2d::Ref *sender)
{
	int i = dynamic_cast<MenuItemFont*>(sender)->getTag();
	auto const &product = _products[i];
	CCLOG("Start IAP %s", product.name.c_str());
	sdkbox::IAP::purchase(product.name);
}

//------------------------------------------------------------------------------------
void AdsManager::onInitialized(bool ok)
{
	CCLOG("%s : %d", __func__, ok);
}

//------------------------------------------------------------------------------------
void AdsManager::onSuccess(const sdkbox::Product &p)
{
	CCLOG("onStart IAP %s", p.name.c_str());
	if (p.name == REMOVE_ADS)
	{
		UserDefault::getInstance()->setBoolForKey("remove_ad", true);
		remove_ad = true;
	}else if(p.name == UNLOCK_ALL_LEVELS){
		UserDefault::getInstance()->setBoolForKey("unlock_all", true);
		unlock_all = true;
	}

    EventCustom e(EVENT_BUY_SUCCESS);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&e);

}

//------------------------------------------------------------------------------------
void AdsManager::onFailure(const sdkbox::Product &p, const std::string &msg)
{
	CCLOG("Purchase Failed: %s", msg.c_str());
	if (p.name == REMOVE_ADS) {
		UserDefault::getInstance()->setBoolForKey("remove_ad", false);
		remove_ad = false;
	}
	else if (p.name == UNLOCK_ALL_LEVELS) {
		UserDefault::getInstance()->setBoolForKey("unlock_all", false);
		unlock_all = false;
	}

    EventCustom e(EVENT_BUY_FAILED);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&e);
}

//------------------------------------------------------------------------------------
void AdsManager::onCanceled(const sdkbox::Product &p)
{
	CCLOG("Purchase Canceled: %s", p.id.c_str());

	if (p.name == REMOVE_ADS) {
		UserDefault::getInstance()->setBoolForKey("remove_ad", false);
		remove_ad = false;
	}
	else if (p.name == UNLOCK_ALL_LEVELS) {
		UserDefault::getInstance()->setBoolForKey("unlock_all", false);
		unlock_all = false;
	}
    EventCustom e(EVENT_BUY_CANCEL);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&e);
}

//------------------------------------------------------------------------------------
void AdsManager::onRestored(const sdkbox::Product& p)
{
	CCLOG("Purchase Restored: %s", p.name.c_str());
	if(p.name == REMOVE_ADS){
        //CCLOG("Purchase Restored --------------: %s", p.name.c_str());
        UserDefault::getInstance()->setBoolForKey("remove_ad", true);
        remove_ad = true;
	}
	else if(p.name == UNLOCK_ALL_LEVELS)
    {
        UserDefault::getInstance()->setBoolForKey("unlock_all", true);
        unlock_all = true;

    }
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    if (!remove_ad || !unlock_all) {
        EventCustom e(EVENT_RT_SUCCESS);
        Director::getInstance()->getEventDispatcher()->dispatchEvent(&e);
    }
#else
    EventCustom e(EVENT_RT_SUCCESS);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&e);
#endif
    
}

//------------------------------------------------------------------------------------
void AdsManager::updateIAP(const std::vector<sdkbox::Product>& products)
{
	/*_iapMenu->removeAllChildren();
	_products = products;


	for (int i=0; i < _products.size(); i++)
	{
	CCLOG("IAP: ========= IAP Item =========");
	CCLOG("IAP: Name: %s", _products[i].name.c_str());
	CCLOG("IAP: ID: %s", _products[i].id.c_str());
	CCLOG("IAP: Title: %s", _products[i].title.c_str());
	CCLOG("IAP: Desc: %s", _products[i].description.c_str());
	CCLOG("IAP: Price: %s", _products[i].price.c_str());
	CCLOG("IAP: Price Value: %f", _products[i].priceValue);
	CCLOG("IAP: Currency: %s", _products[i].currencyCode.c_str());

	auto item = MenuItemFont::create(_products[i].name, CC_CALLBACK_1(FirstScene::onIAP, this));
	//item->setUserData(reinterpret_cast<void*>(i));
	item->setTag(i);
	_iapMenu->addChild(item);
	}

	Size size = Director::getInstance()->getWinSize();
	_iapMenu->alignItemsVerticallyWithPadding(5);
	_iapMenu->setPosition(Vec2(0, - size.height / 4));*/
}

//------------------------------------------------------------------------------------
void AdsManager::onProductRequestSuccess(const std::vector<sdkbox::Product> &products)
{
	updateIAP(products);
}

//------------------------------------------------------------------------------------
void AdsManager::onProductRequestFailure(const std::string &msg)
{
	CCLOG("Fail to load products");
}

//------------------------------------------------------------------------------------
void AdsManager::onRestoreComplete(bool ok, const std::string &msg)
{
	CCLOG("#on %s:%d:%s", __func__, ok, msg.data());
}

#endif
