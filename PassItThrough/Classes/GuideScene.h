/************************************************************************
*  GuideScene.h														
*																		
*																		
*  Created by Lin dao min									
*  Copyright Lin 2014-4-5											
*																		
************************************************************************/

#ifndef __GUIDE_SCENE_H__
#define __GUIDE_SCENE_H__

#include "common.h"

#include <iostream>
#include <stdlib.h>
#include <string>

//#include "atlbase.h"
//#include "atlstr.h"
//#include "comutil.h"


class GuideScene: public cocos2d::Scene
{
public: 
	GuideScene();
	virtual ~GuideScene();
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();
	static GuideScene* create();

	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);

	void backCallBack(Ref* pSender);

//pause layer button method 
	
	void onRotate(Ref* pSender);
	
	void TapMoveAnimation();
	
	void initWire();
	
	int RandomBetweenAandB(int a,int b);
	
	void DragonGeneration(); 
	
	void DragonAppear(float dt);

	int vibrate();

	void showGameOver(bool bWin);
	void drawBonus(Vec2 pos);       // score 10+ animation
	void removeBonus(Node* sender); 
	void nextChapter(Node* sender);
	
	void sparkAnimation(float dt);
	void sparkAnimation1(float dt);
	
	void sparkFunc(Node* pNode);
	void sparkFunc1(Node* pNode);
	void nextMoveAnimation(Node* pNode);
	void finalMoveAnimation(Node* pNode);
	void endPhase(Node* pNode);
	
	PointArray *m_pArray;  

public:
	bool bIsCollision;
	Sprite* m_pRingRight;
	Sprite* m_pRingLeft;
	Sprite* m_pRingUp;
	Sprite* m_pRingDown;
	Sprite* m_pRingUpTemp;
	Sprite* m_pRingDownTemp;


	Sprite* m_pWire[30];
	Sprite *rightHand;
	Sprite *leftHand;
	Sprite* m_DirectPoint;
	bool DirectPointState;
	Sprite* m_pTapPoint;
	Sprite* m_pTapCopy;
	Sprite* m_pText1;
	Sprite* m_pText2;
	bool tapPointState;
	Sprite* m_pTap;
	Sprite* m_pLoseMessage;
	Sprite* m_pWinMessage;
	Sprite* m_pSignBoard;

	Label* lb_score;
	cocos2d::Vector<Sprite*> dragon_array;
	
/////main game 
	MenuItemImage* m_pReplayItem;
	MenuItemImage* m_pReturnHome;
	MenuItemImage* m_pReturnSelectlevel;
	MenuItemImage* m_pRightButton;
	MenuItemImage* m_pLeftButton;
	MenuItemImage* m_pPauseItem;

	Sprite* button_between;

	////game over layer
	Sprite* m_pGameOverBoard;
	Menu* pGameOverMenu;

	Sprite* m_pMusicOn;
	Sprite* m_pMusicOff;
	Sprite* m_pSoundOn;
	Sprite* m_pSoundOff;

	Menu * pMenu;

	Vec2 curPoint;
	Vec2 touchStart;
	Vec2 moveOldPoint;
	float fDistance;
	bool touchStatus;
	Vec2 initTapPos;
	Vec2 initRingPos;
	Vec2 initRingUpPos;
	Vec2 initRingDownPos;
	int touch_kind;
	int pattern_width;
	int pattern_height;
	int num;   //////////////////////////////////////////////////////////////////////////curve num in selected chapter

public:
	
	int m_nScore;
	int dragon_num;
	float dragon_width;
	int index;
	int index1;
	bool win_or_lose;
	
};

#endif  // __GUIDE_SCENE_H__