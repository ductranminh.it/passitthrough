#include "SettingScene.h"
//#include "SelectLevelScene.h"
//#include "GameData.h"
//#include "GuideScene.h"
#include "MainGameScene.h"
#include "adsManager.h"
#include "FirstScene.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>

#endif

using namespace cocos2d;
using namespace CocosDenshion;

enum enSettingSceneMenu{

	enFacebook				= 1,
	enTwitter				= 2,
	enOk					= 3,
	enAbout					= 4,
	enBackgroundMusicOn		= 5,
	enBackgroundMusicOff    = 6,
	enSoundOn               = 7,
	enSoundOff              = 8,
	enClose                 = 9,
	enPlus					= 10,
	enMinus					= 11,
	enMenu

};
#define enMusicVolume 300

SettingScene* SettingScene::create(){

	SettingScene* scene = new SettingScene;
	scene->init();
	scene->autorelease();
	return scene;
}

SettingScene::SettingScene(){

	SimpleAudioEngine::getInstance()->preloadEffect("sound/falling.mp3");
	SimpleAudioEngine::getInstance()->preloadEffect("sound/jumping.mp3");
	SimpleAudioEngine::getInstance()->preloadEffect("sound/button.mp3");
}

SettingScene::~SettingScene(){

	SimpleAudioEngine::getInstance()->unloadEffect("sound/falling.mp3");
	SimpleAudioEngine::getInstance()->unloadEffect("sound/jumping.mp3");
	SimpleAudioEngine::getInstance()->unloadEffect("sound/button.mp3");
}

Scene* SettingScene::createScene(){

	return SettingScene::create();
}

// on "init" you need to initialize your instance
bool SettingScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}

	initBackground();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(SettingScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(SettingScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(SettingScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	cocos2d::Device::setKeepScreenOn(true);

	return true;
}

void SettingScene::initBackground(){
	//background
	setting_background = Sprite::create("option_screen/broad.png");
	CommonUtiles::setAdjustScale(setting_background);
 	setting_background->setPosition( Vec2(winSize.width *0.5f, winSize.height * 0.5f));
	setting_background->setScale(setting_background->getScale()*0.4f);
	addChild(setting_background,2);
	float m_nWidth=setting_background->getContentSize().width;
	float m_nHeight=setting_background->getContentSize().height;

	auto lb_sound= Label::createWithTTF("Sound",fontList[3],60,Size(winSize.width * 0.5f, winSize.height * 0.25f), TextHAlignment::CENTER);
	lb_sound->setVerticalAlignment(TextVAlignment::CENTER);
	lb_sound->setPosition( Vec2(m_nWidth*0.25f, m_nHeight*0.6f));
	lb_sound->setColor(Color3B(255,255,255));
	setting_background->addChild(lb_sound,1);

	auto lb_music= Label::createWithTTF("Music",fontList[3],60,Size(winSize.width * 0.5f, winSize.height * 0.25f), TextHAlignment::CENTER);
	lb_music->setVerticalAlignment(TextVAlignment::CENTER);
	lb_music->setPosition( Vec2(m_nWidth*0.25f, m_nHeight*0.355f));
	lb_music->setColor(Color3B(255,255,255));
	setting_background->addChild(lb_music,1);

	//music bar
	music_bar=Sprite::create("option_screen/music_bar.png");
	music_bar->setPosition(Vec2(m_nWidth*0.65f, m_nHeight*0.35f));
	setting_background->addChild(music_bar,1);

	//sound bar
	sound_bar=Sprite::create("option_screen/sound_bar.png");
	sound_bar->setPosition(Vec2(m_nWidth*0.65f, m_nHeight*0.6f));

	setting_background->addChild(sound_bar,1);

	//sound tap
	
	sound_tap=Sprite::create("option_screen/button-sound.png");
	sound_tap->setTag(enSoundOn);
	sound_tap->setPosition(Vec2(m_nWidth*0.65f, m_nHeight*0.6f));
	setting_background->addChild(sound_tap,4);
	
	//game menu
	MenuItemImage* btn_facebook = MenuItemImage::create("button/facebook.png", "button/facebook.png", CC_CALLBACK_1(SettingScene::backCallBack, this));
	btn_facebook->setTag(enFacebook);	
	btn_facebook->setPosition(Vec2(m_nWidth*0.55f, m_nHeight*0.32f));

	MenuItemImage* btn_twitter = MenuItemImage::create("button/twitter.png", "button/twitter.png", CC_CALLBACK_1(SettingScene::backCallBack, this));
	btn_twitter->setTag(enTwitter);
	btn_twitter->setPosition(Vec2(m_nWidth*0.8f, m_nHeight*0.32f));

	//ok menu
	MenuItemImage* btn_ok = MenuItemImage::create("option_screen/ok.png", "option_screen/ok.png", CC_CALLBACK_1(SettingScene::backCallBack, this));
	btn_ok->setTag(enOk);
	btn_ok->setPosition(Vec2(m_nWidth*0.75f, m_nHeight*0.05f));

	//cancel
	MenuItemImage* btn_cancel = MenuItemImage::create("option_screen/close.png", "option_screen/close.png", CC_CALLBACK_1(SettingScene::backCallBack, this));
	btn_cancel->setTag(enClose);
	btn_cancel->setPosition(Vec2(m_nWidth*0.25f, m_nHeight*0.05f));
	//volume up
	MenuItemImage* btn_plus = MenuItemImage::create("option_screen/music-up.png", "option_screen/music-up.png", CC_CALLBACK_1(SettingScene::backCallBack, this));
	btn_plus->setTag(enPlus);

	btn_plus->setPosition(Vec2(m_nWidth*0.83f, m_nHeight*0.35f));

	//volume down
	MenuItemImage* btn_minus = MenuItemImage::create("option_screen/music-down.png", "option_screen/music-down.png", CC_CALLBACK_1(SettingScene::backCallBack, this));
	btn_minus->setTag(enMinus);

	btn_minus->setPosition(Vec2(m_nWidth*0.47f, m_nHeight*0.35f));

	m_pMainMenu=Menu::create(btn_ok,btn_cancel,btn_plus,btn_minus,NULL);	
	m_pMainMenu->setPosition(Vec2::ZERO);
	m_pMainMenu->setTag(enMenu);
	setting_background->addChild(m_pMainMenu,3);

	if(music_state_changed){

		if(music_state){
			music_volume=8.f;
		}
		else{
			music_volume=0.f;
		}
	}
	else{
		music_volume=UserDefault::getInstance()->getFloatForKey("music_volume",4.0f);
	}
	
	DrawMusicVolume(music_volume);

	if(sound_state_changed){
		if(sound_state){
			sound_volume=10.f;
		}
		else{
			sound_volume=0.f;
		}
	}
	else
		sound_volume=UserDefault::getInstance()->getFloatForKey("sound_volume",5.0f);

	float startx=sound_bar->getPositionX()-sound_bar->getContentSize().width/2.0f+40.f;
	float endx=sound_bar->getPositionX()+sound_bar->getContentSize().width/2.0f-40.f;
	sound_tap->setPositionX(startx+sound_volume*((endx-startx)/10.f));
	
	touch_status=false;
	instruction_state=false;
}

void SettingScene::DrawMusicVolume(int num){

	if(num<0) return;
	for(int i=0;i<20;i++){
		setting_background->removeChildByTag(enMusicVolume+i);
	}
	float startx=music_bar->getBoundingBox().getMinX()+39.f;
	for(int i=0;i<num;i++){
		Sprite* music_vol=Sprite::create("option_screen/music-element.png");
		music_vol->setPosition(Vec2(startx+music_vol->getContentSize().width*i+20.f,music_bar->getPositionY()));
		music_vol->setTag(enMusicVolume+i);
		setting_background->addChild(music_vol,6);
	}
}

void SettingScene::backCallBack(Ref* pSender){
	switch(((MenuItem *)pSender)->getTag())
	{
	case enFacebook:	
		{
			AdsManager::getInstance()->OpenURL("https://www.facebook.com/sharer/sharer.php?u=https://play.google.com/store/apps/details?id=com.magez.passitthrough");

			break;
		}
	case enTwitter:		
		{
			int level=UserDefault::getInstance()->getIntegerForKey("total_complete_chapter",0);
			int score=UserDefault::getInstance()->getIntegerForKey("score",0);
			AdsManager::getInstance()->postTW(level,score);
			break;
		}
	case enOk:			
		{
			UserDefault::getInstance()->setFloatForKey("sound_volume",sound_volume);
			UserDefault::getInstance()->setFloatForKey("music_volume",music_volume);	
			UserDefault::getInstance()->flush();
			SimpleAudioEngine::getInstance()->setBackgroundMusicVolume( music_volume/8.f );
			SimpleAudioEngine::getInstance()->setEffectsVolume(sound_volume/10.f);
			this->getParent()->resume();
			
			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();
			//this->getParent()->removeChild(setting_background);
			
			break;
		}
	case enClose:			
		{
			sound_volume=UserDefault::getInstance()->getFloatForKey("sound_volume",5.0f);
			music_volume=UserDefault::getInstance()->getFloatForKey("music_volume",4.0f);

			SimpleAudioEngine::getInstance()->setBackgroundMusicVolume( music_volume/8.f );
			SimpleAudioEngine::getInstance()->setEffectsVolume(sound_volume/10.f);
			
			this->getParent()->resume();

			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();
			//this->getParent()->removeChild(setting_background);

			break;
		}

	case enPlus:			
		{
			music_state_changed=false;
			music_volume++;
			if(music_volume>8)
				music_volume=8;
			DrawMusicVolume(music_volume);
			if(music_state)
			{
				SimpleAudioEngine::getInstance()->setBackgroundMusicVolume( music_volume/8.f );
				
			}
			
			break;
		}
	case enMinus:			
		{
			music_state_changed=false;
			music_volume--;
			if(music_volume<0)
				music_volume=0;
			DrawMusicVolume(music_volume);
			if(music_state)
			{
				SimpleAudioEngine::getInstance()->setBackgroundMusicVolume( music_volume/8.f );

			}
			break;
		}
	case enAbout:
		{
			
			break;
		}
	}
}

bool SettingScene::onTouchBegan(Touch* touch, Event* event){

	touchStart = setting_background->convertTouchToNodeSpace(touch);
	if (sound_tap->getBoundingBox().containsPoint(touchStart) && !touch_status){
		touch_status=true;
		return true;
	}
	
	return false;
}

void SettingScene::onTouchMoved(Touch* touch, Event* event){

	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
	sound_state_changed=false;
	float startx=sound_bar->getPositionX()-sound_bar->getContentSize().width/2.0f+40.f;
	float endx=sound_bar->getPositionX()+sound_bar->getContentSize().width/2.0f-40.f;
	if(curPos.x>=startx && curPos.x<=endx){

		sound_tap->setPositionX(curPos.x);
		sound_volume=(curPos.x-startx)/((endx-startx)/10.f);
		if(sound_state)
			SimpleAudioEngine::getInstance()->setEffectsVolume(sound_volume/10.f);
	}
}

void SettingScene::onTouchEnded(Touch* touch, Event* event){

	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
	touch_status=false;
}

