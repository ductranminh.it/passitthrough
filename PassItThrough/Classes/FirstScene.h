/************************************************************************
*  FirstScene.h															
*																		
*																		
*  Created by udinsoft									
*  Copyright Lin 2019-5-14											
*																		
************************************************************************/

#ifndef __FIRST_SCENE_H__
#define __FIRST_SCENE_H__

#include "common.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    #define SDKBOX_ENABLED 1
    #ifdef SDKBOX_ENABLED
    #include "PluginShare/PluginShare.h"
    #endif
class FirstScene: public cocos2d::Scene, public sdkbox::ShareListener
#else
	class FirstScene : public cocos2d::Scene
#endif
{
public:
	FirstScene();
	virtual ~FirstScene();
	virtual bool init() override;

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();
	static FirstScene* create();

	void keyBackClicked();

	void backCallBack(Ref* pSender);
	void onLightEffect(float dt);

	Sprite* getLightAnimation();
	void OnTimerLoading(float dt);
	void showMainMenu();
	void showMainScreen();
	void playBtnClkSound();
	void InitBackground(float dt);
	void onTimer(float dt);
	void showRateScene();
	void showRemoveAdScene();

    void execute(EventCustom* event);

	//CREATE_FUNC(FirstScene);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    void afterCaptureScreen(bool yes, const std::string &outputFilename);

#endif

	//void removeAd();

private:
	Sprite* m_pBackground;
	Sprite* m_pBackground1;
	Sprite* m_pBgAnimation;

	MenuItemSprite* m_pPlayItem;
	MenuItemSprite* m_pScoreBoardItem;
	MenuItemSprite* m_pSettingItem;
	MenuItemSprite* m_pAboutItem;
	MenuItemSprite* m_pExitItem;
	MenuItemSprite* m_pFacebookItem;
	MenuItemSprite* m_pTwitterItem;
	MenuItemSprite* m_pGoogleItem;
	MenuItemSprite* m_pIAPItem;
	MenuItemSprite* m_pRemoveAdItem;
    MenuItemSprite* m_pRestoreItem;
	Menu * pMenu;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID ) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)

    std::string _captureFilename;

    virtual void onShareState(const sdkbox::SocialShareResponse& response) override ;

#endif


};


#endif  // __FIRST_SCENE_H__
