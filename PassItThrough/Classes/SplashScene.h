/************************************************************************
*  SplashScene.h														
*																		
*																		
*  Created by Lin daomin											
*  Copyright Lin 2014 - 5 - 24											
*																		
************************************************************************/

#ifndef __SPLASH_SCENE_H__
#define __SPLASH_SCENE_H__
#include "common.h"

class SplashScene: public cocos2d::Scene
{
public:

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	
	virtual bool init();

	void onLoadingEnd(Node *node);
	void onTimer(float dt);

	// implement the "static create()" method manually
	CREATE_FUNC(SplashScene);

public:
	Sprite* m_pTap;					// loading progress background
	ProgressTimer* m_TemperatureProgressBar;		// loading progress timer 

	int bar_length;


};


#endif  // __SPLASH_SCENE_H__