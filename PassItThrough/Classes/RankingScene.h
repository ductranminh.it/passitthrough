
#ifndef __RANKING_SCENE_H__
#define __RANKING_SCENE_H__

#include "common.h"

class  RankingScene: public cocos2d::Scene
{
public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();

	// implement the "static node()" method manually
	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);
	
	void backCallBack(Ref* pSender);
	
	void initBackground();
	void getValue();
	CREATE_FUNC(RankingScene);
	
public:
	float zoomx;
	float zoomy;
	Menu* m_pMainMenu;
//	bool soundon;
	bool instruction_state;
	Sprite* sprite;
	bool BackgroundMusicOn;
	bool SoundOn;
	Point touchStart;
public:
	Sprite *setting_background;
	
	bool touch_status;

	Label* rank_num[5];
	Label* rank_score[5];
	Label* rank_date[5];

};



#endif  // __RANKING_SCENE_H__