/************************************************************************
*  GuideScene.cpp														
*																		
*																		
*  Created by Lin dao min									
*  Copyright Lin 2014-4-5											
*																		
************************************************************************/
//#include <Winnls.h>
#include "GuideScene.h"
#include "FirstScene.h"
#include "SelectLevelScene.h"
#include "MainGameScene.h"
#include "adsManager.h"
#include "CloseScene.h"

enum enGameOverMenu
{
	enGameOverMenu_Stage			=1,
	enGameOverMenu_Retry			=2,
	enGameOverMenu_Next				=3,
	enGameOverMenu_Facebook			=4,
	enGameOverMenu_Twitter          =5,
	enGameOverMenu_Google			=6,
	
};

using namespace cocos2d;
using namespace CocosDenshion;

GuideScene* GuideScene::create() {
	auto scene = new GuideScene;
	scene->init();
	scene->autorelease();
	return scene;
}

GuideScene::GuideScene(){
	SimpleAudioEngine::getInstance()->preloadEffect("sound/alert.mp3");
}

GuideScene::~GuideScene(){
	SimpleAudioEngine::getInstance()->unloadEffect("sound/alert.mp3");
}

Scene* GuideScene::createScene(){
	return GuideScene::create();
}

// on "init" you need to initialize your instance
bool GuideScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}

	char buf[64];
	sprintf(buf, "Round %d", 1);

	auto lb_chapter = Label::createWithTTF(buf, fontList[3], 40, Size(winSize.width * 0.6f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_chapter->setVerticalAlignment(TextVAlignment::CENTER);
	lb_chapter->setPosition(Vec2(winSize.width*0.12f, winSize.height*0.07f));
	lb_chapter->setColor(Color3B(254, 201, 33));
	addChild(lb_chapter, 1);

	///////// background

	Sprite* pBg;
	float nWidth = winSize.width;
	float nHeight = winSize.height;

	pBg = Sprite::create("common/bg1_1280x800.png");
	pBg->setScaleX(winSize.width / pBg->getContentSize().width * fScaleRetina);
	pBg->setScaleY(winSize.height / pBg->getContentSize().height * fScaleRetina);
	pBg->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));

	addChild(pBg, 0);
	pBg->setOpacity(180);

	//setting button
	m_pReturnHome = MenuItemImage::create("button/icon-home.png", "button/icon-home.png", CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pReturnHome->setScale(m_pReturnHome->getScale() * 0.6f);
	m_pReturnHome->setPosition(Vec2(winSize.width * 0.06f, winSize.height * 0.9f));

	////pause button
	m_pPauseItem = MenuItemImage::create("button/pause.png", "button/pause.png", CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pPauseItem->setScale(m_pPauseItem->getScale() * 0.5f);
	m_pPauseItem->setPosition(Vec2(winSize.width * 0.9f, winSize.height * 0.9f));

	m_pReturnSelectlevel = MenuItemImage::create("button/icon-home.png", "button/icon-home.png", CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pReturnSelectlevel->setScale(m_pReturnSelectlevel->getScale() * 0.6f);
	m_pReturnSelectlevel->setPosition(Vec2(winSize.width * 0.3f, winSize.height * 0.9f));

	/////right and left button

	button_between = Sprite::create("button/left_right_button.png");
	button_between->setScale(button_between->getScale()*0.3f);
	button_between->setPosition(Vec2(winSize.width * 0.9f, winSize.height * 0.18f));
	addChild(button_between, 2);

	Vec2 pos = button_between->getPosition();

	m_pLeftButton = MenuItemImage::create("button/button_left.png", "button/button_left.png", CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pLeftButton->setScale(m_pLeftButton->getScale()*0.3f);
	m_pLeftButton->setAnchorPoint(Vec2(1.f, 0.5f));
	m_pLeftButton->setPosition(Vec2(pos.x - 10.f*fScaleN*fScaleRetina, pos.y));

	m_pRightButton = MenuItemImage::create("button/button_left.png", "button/button_left.png", CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pRightButton->setScale(m_pRightButton->getScale()*0.3f);
	m_pRightButton->setRotation(180);
	m_pRightButton->setAnchorPoint(Vec2(1.f, 0.5f));
	m_pRightButton->setPosition(Vec2(pos.x + 10.f*fScaleN*fScaleRetina, pos.y));

	m_DirectPoint = Sprite::create("guide/effect-2.png");
	m_DirectPoint->setScale(m_DirectPoint->getScale()*0.4f);
	m_DirectPoint->setPosition(Vec2(winSize.width * 0.9f, winSize.height * 0.2f));
	addChild(m_DirectPoint, 3);
	m_DirectPoint->setVisible(false);

	DirectPointState = false;

	pMenu = Menu::create(m_pPauseItem, m_pReturnHome, m_pLeftButton, m_pRightButton, NULL);
	pMenu->setPosition(Vec2::ZERO);
	addChild(pMenu, 1);


	/////////music and sound button

	m_pMusicOff = Sprite::create("button/icon-music-off.png");
	m_pMusicOff->setScale(m_pMusicOff->getScale()*0.5f);

	m_pMusicOff->setPosition(Vec2(winSize.width * 0.75f, winSize.height * 0.9f));
	addChild(m_pMusicOff, 3);

	m_pMusicOn = Sprite::create("button/icon-music-on.png");
	CommonUtiles::setAdjustScale(m_pMusicOn);
	m_pMusicOn->setScale(m_pMusicOn->getScale()*0.5f);
	m_pMusicOn->setPosition(Vec2(winSize.width * 0.75f, winSize.height * 0.9f));
	addChild(m_pMusicOn, 3);
	if (!music_state)
		m_pMusicOn->setVisible(false);

	m_pSoundOff = Sprite::create("button/icon-sound-off.png");
	CommonUtiles::setAdjustScale(m_pSoundOff);
	m_pSoundOff->setScale(m_pSoundOff->getScale()*0.5f);
	m_pSoundOff->setPosition(Vec2(winSize.width * 0.82f, winSize.height * 0.9f));
	addChild(m_pSoundOff, 3);

	m_pSoundOn = Sprite::create("button/icon-sound-on.png");
	CommonUtiles::setAdjustScale(m_pSoundOn);
	m_pSoundOn->setScale(m_pSoundOn->getScale()*0.5f);
	m_pSoundOn->setPosition(Vec2(winSize.width * 0.82f, winSize.height * 0.9f));
	addChild(m_pSoundOn, 3);
	if (!sound_state)
		m_pSoundOn->setVisible(false);

	// ring image
	Sprite* temp = Sprite::create("common/ring2.png");
	Size tempSize = temp->getContentSize();

	m_pRingRight = Sprite::create("common/ring2.png", Rect(tempSize.width / 2, 0, tempSize.width / 2, tempSize.height));
	CommonUtiles::setAdjustScale(m_pRingRight);
	m_pRingRight->setAnchorPoint(Vec2(0.f, 0.5f));
	m_pRingRight->setScale(m_pRingRight->getScale() * 1.8f);
	m_pRingRight->setPosition(Vec2(winSize.width*0.08f, winSize.height * 0.58f));
	addChild(m_pRingRight, 4);

	m_pRingLeft = Sprite::create("common/ring2.png", Rect(0, 0, tempSize.width / 2, tempSize.height));
	CommonUtiles::setAdjustScale(m_pRingLeft);
	m_pRingLeft->setAnchorPoint(Vec2(1.f, 0.5f));
	m_pRingLeft->setScale(m_pRingLeft->getScale() * 1.8f);
	m_pRingLeft->setPosition(m_pRingRight->getPosition());
	addChild(m_pRingLeft, 1);

	////////////// top part of ring for collision checking 
	m_pRingUp = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingUp);
	m_pRingUp->setAnchorPoint(Vec2(0.5f, 0.5f));

	m_pRingUp->setPosition(Vec2(0, tempSize.height - (m_pRingUp->getBoundingBox().getMaxY() - m_pRingUp->getBoundingBox().getMinY()) / 2.f));		//0.62
	m_pRingRight->addChild(m_pRingUp, 5);
	m_pRingUp->setFlippedY(true);
	m_pRingUp->setOpacity(1);


	// bottom part of ring for collision checking 
	m_pRingDown = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingDown);
	m_pRingDown->setAnchorPoint(Vec2(0.5f, 0.5f));

	m_pRingDown->setPosition(Vec2(0, (m_pRingUp->getBoundingBox().getMaxY() - m_pRingUp->getBoundingBox().getMinY()) / 2.f));		//0.62
	m_pRingRight->addChild(m_pRingDown, 5);

	m_pRingDown->setOpacity(1);

	////////////////////////---temple
	m_pRingUpTemp = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingUpTemp);
	m_pRingUpTemp->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pRingUpTemp->setScale(m_pRingUpTemp->getScale() * 1.8f);
	m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));

	addChild(m_pRingUpTemp, 5);
	m_pRingUpTemp->setFlippedY(true);
	m_pRingUpTemp->setOpacity(1);


	// bottom part of ring for collision checking 
	m_pRingDownTemp = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingDownTemp);
	m_pRingDownTemp->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pRingDownTemp->setScale(m_pRingDownTemp->getScale() * 1.8f);
	m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));//0.62
	
	addChild(m_pRingDownTemp, 5);
	m_pRingDownTemp->setOpacity(1);


	// tap image
	m_pTap = Sprite::create("common/handle.png");
	CommonUtiles::setAdjustScale(m_pTap);
	m_pTap->setScale(m_pTap->getScale() * 0.6f);
	m_pTap->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pTap->setPosition(Vec2(winSize.width*0.08f, winSize.height * 0.37f));
	addChild(m_pTap, 4);

	m_pTapCopy = Sprite::create("common/handle.png");
	CommonUtiles::setAdjustScale(m_pTapCopy);
	m_pTapCopy->setScale(m_pTapCopy->getScale() * 0.6f);
	m_pTapCopy->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pTapCopy->setPosition(Vec2(winSize.width*0.08f, winSize.height * 0.37f));
	addChild(m_pTapCopy, 5);
	m_pTapCopy->setVisible(false);
	tapPointState = false;

	m_pTapPoint = Sprite::create("guide/effect-1.png");
	CommonUtiles::setAdjustScale(m_pTapPoint);
	m_pTapPoint->setScale(m_pTapPoint->getScale() * 0.6f);
	m_pTapPoint->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pTapPoint->setPosition(Vec2(winSize.width*0.08f, winSize.height * 0.37f));
	addChild(m_pTapPoint, 5);
	m_pTapPoint->setVisible(false);

	///guide text
	m_pText1 = Sprite::create("guide/text1.png");
	CommonUtiles::setAdjustScale(m_pText1);
	m_pText1->setScale(m_pText1->getScale() * 0.6f);
	m_pText1->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pText1->setPosition(Vec2(winSize.width*0.28f, winSize.height * 0.35f));
	addChild(m_pText1, 5);
	m_pText1->setVisible(false);

	m_pText2 = Sprite::create("guide/text2.png");
	CommonUtiles::setAdjustScale(m_pText2);
	m_pText2->setScale(m_pText2->getScale() * 0.6f);
	m_pText2->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pText2->setPosition(Vec2(winSize.width*0.7f, winSize.height * 0.18f));
	addChild(m_pText2, 5);
	m_pText2->setVisible(false);


	// lose message
	m_pLoseMessage = Sprite::create("common/lose_panel.png");
	CommonUtiles::setAdjustScale(m_pLoseMessage);
	m_pLoseMessage->setScale(m_pLoseMessage->getScale() * 0.4f);
	m_pLoseMessage->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	addChild(m_pLoseMessage, 5);
	m_pLoseMessage->setVisible(false);

	m_pSignBoard = Sprite::create("common/signbroad.png");
	CommonUtiles::setAdjustScale(m_pSignBoard);
	m_pSignBoard->setScale(m_pSignBoard->getScale() * 0.4f);
	m_pSignBoard->setAnchorPoint(Vec2(0.5f, 1.f));
	m_pSignBoard->setPosition(Vec2(winSize.width*0.5f, winSize.height));
	addChild(m_pSignBoard, 5);

	//score title and score num display
	auto lb_score_title = Label::createWithTTF("Score", fontList[3], 80, Size(winSize.width * 0.5f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_score_title->setVerticalAlignment(TextVAlignment::CENTER);
	lb_score_title->setPosition(Vec2(m_pSignBoard->getContentSize().width*0.5f, m_pSignBoard->getContentSize().height*0.63f));
	lb_score_title->setColor(Color3B(254, 201, 33));
	m_pSignBoard->addChild(lb_score_title, 1);


	m_nScore = UserDefault::getInstance()->getIntegerForKey("score", 0);
	sprintf(buf, "%d", m_nScore);
	lb_score = Label::createWithTTF(buf, fontList[3], 80, Size(winSize.width * 0.5f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_score->setVerticalAlignment(TextVAlignment::CENTER);
	lb_score->setPosition(Vec2(m_pSignBoard->getContentSize().width*0.5f, m_pSignBoard->getContentSize().height*0.3f));
	lb_score->setColor(Color3B(254, 201, 33));
	m_pSignBoard->addChild(lb_score, 1);

	index = 0;
	index1 = 0;

	touchStatus = false;

	initWire();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(GuideScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(GuideScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(GuideScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	return true;
}

int GuideScene::RandomBetweenAandB(int a,int b){

	if(a>b){
		int c=b;
		b=a;
		a=c;
	}
	return a+rand()%(b-a+1);
}
void GuideScene::initWire(){

	num=6;
	int wire_index[6]={1,3,4,5,6,37};
	float zoomx=0.7f;

	// wire spline
	int item;
	for(int order=0;order<=num-1;order++){
		item=wire_index[order];
		char buf[256];
		//CCLog("wire_num=%d",item);
		sprintf(buf,"curve/curve%d.png",item);
		m_pWire[order] = Sprite::create(buf);
		CommonUtiles::setAdjustScale(m_pWire[order]);
		m_pWire[order]->setScale(m_pWire[order]->getScale() *zoomx);		
		m_pWire[order]->setAnchorPoint(Vec2(0.f,0.5f));
		pattern_width=m_pWire[order]->getBoundingBox().getMaxX()-m_pWire[order]->getBoundingBox().getMinX();
		pattern_height=m_pWire[order]->getContentSize().height;
		m_pWire[order]->setPosition(Vec2(pattern_width*order, winSize.height * 0.6f));
		addChild(m_pWire[order], 2);
	}
	
	TapMoveAnimation();
	schedule(schedule_selector(GuideScene::DragonAppear),0.1f);
}

void GuideScene::TapMoveAnimation(){

	//hand move
	rightHand=Sprite::create("guide/hand.png");
	CommonUtiles::setAdjustScale(rightHand);
	rightHand->setScale(rightHand->getScale()*0.3f);
	rightHand->setPosition(Vec2(winSize.width*0.4f,-winSize.height*0.2f));
	rightHand->setAnchorPoint(Vec2(0.f,1.f));
	MoveTo *handMove=MoveTo::create(2.f,m_pTap->getPosition());
	rightHand->runAction(Sequence::create(handMove,DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(GuideScene::sparkFunc,this,rightHand)),NULL));
	addChild(rightHand,5);

	m_pText1->runAction(Sequence::create(DelayTime::create(6.f), CallFunc::create(CC_CALLBACK_0(GuideScene::nextMoveAnimation,this,m_pText1)),NULL));
	
	leftHand=Sprite::create("guide/hand2.png");
	CommonUtiles::setAdjustScale(leftHand);
	CommonUtiles::setAdjustScale(leftHand);
	leftHand->setScale(leftHand->getScale()*0.3f);
	leftHand->setPosition(Vec2(winSize.width*0.6f,-winSize.height*0.2f));
	leftHand->setAnchorPoint(Vec2(0.5f,1.f));
	MoveTo *handMove1=MoveTo::create(1.f,button_between->getPosition());
	leftHand->runAction(Sequence::create(DelayTime::create(7.f),handMove1,DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(GuideScene::sparkFunc1,this,leftHand)),NULL));
	addChild(leftHand,5);
}

void GuideScene::sparkFunc(Node* pNode){

	schedule(schedule_selector(GuideScene::sparkAnimation),0.3f);
}

void GuideScene::sparkFunc1(Node* pNode){

	schedule(schedule_selector(GuideScene::sparkAnimation1),0.3f);
}

void GuideScene::sparkAnimation(float dt){

	index1++;
	if(index1>8){

		unschedule(schedule_selector(GuideScene::sparkAnimation ));
		removeChild(m_pTapPoint);
		removeChild(m_pTapCopy);
		m_pText1->setVisible(true);
		index1=0;
		
		return;
	}
	if(tapPointState){
		m_pTapPoint->setVisible(false);
		m_pTapCopy->setVisible(false);
		tapPointState=false;

	}
	else{
		m_pTapPoint->setVisible(true);
		m_pTapCopy->setVisible(true);
		tapPointState=true;
	}
}

void GuideScene::sparkAnimation1(float dt){

	index1++;
	if(index1>8){
		unschedule(schedule_selector(GuideScene::sparkAnimation1 ));
		removeChild(m_DirectPoint);
		m_pText2->setVisible(true);

		m_pRingRight->runAction(Sequence::create(DelayTime::create(0.5f),RotateBy::create(1.0f,60.f),DelayTime::create(0.3f),RotateBy::create(1.f,-50),NULL));
		m_pRingLeft->runAction(Sequence::create(DelayTime::create(0.5f),RotateBy::create(1.0f,60.f),DelayTime::create(0.3f),RotateBy::create(1.f,-50),DelayTime::create(0.5f), CallFunc::create(CC_CALLBACK_0(GuideScene::finalMoveAnimation,this,m_pRingLeft)),NULL));
		return;
	}
	if(DirectPointState){
		m_DirectPoint->setVisible(false);
		DirectPointState=false;

	}
	else{
		m_DirectPoint->setVisible(true);
		DirectPointState=true;
	}
}

void GuideScene::nextMoveAnimation(Node* pNode){

	rightHand->runAction(MoveBy::create(1.f,Vec2(winSize.width*0.1f,-winSize.height*0.12f)));
	m_pRingRight->runAction(MoveBy::create(1.f,Vec2(winSize.width*0.1f,-winSize.height*0.12f)));
	m_pRingLeft->runAction(MoveBy::create(1.f,Vec2(winSize.width*0.1f,-winSize.height*0.12f)));
	m_pTap->runAction(MoveBy::create(1.f,Vec2(winSize.width*0.1f,-winSize.height*0.12f)));
	
}

void GuideScene::finalMoveAnimation(Node* pNode){

	rightHand->runAction(MoveBy::create(0.5f,Vec2(winSize.width*0.05f,0)));
	m_pRingRight->runAction(MoveBy::create(0.5f,Vec2(winSize.width*0.05f,0)));
	m_pRingLeft->runAction(MoveBy::create(0.5f,Vec2(winSize.width*0.05f,0)));
	m_pTap->runAction(Sequence::create(MoveBy::create(0.5f,Vec2(winSize.width*0.05f,0)), CallFunc::create(CC_CALLBACK_0(GuideScene::endPhase,this,m_pTap)),NULL));
}

void GuideScene::endPhase(Node* pNode){
	showGameOver(false);
	m_pTap->runAction(Sequence::create(DelayTime::create(2.f), CallFunc::create(CC_CALLBACK_0(GuideScene::nextChapter,this,m_pTap)),NULL));

}

void GuideScene::DragonAppear(float dt){
	index++;
	if(index%10==0){
		index=0;
		int posx=RandomBetweenAandB(80*fScaleN,winSize.width-80*fScaleN);
		Sprite* m_pDragon=Sprite::create("common/dragon.png");
		CommonUtiles::setAdjustScale(m_pDragon);
		m_pDragon->setScale(m_pDragon->getScale()*0.3f);
		m_pDragon->setPosition(Vec2(posx,10.f));
		addChild(m_pDragon,5);
		dragon_array.pushBack(m_pDragon);
	}

	for(int i=0;i<dragon_array.size();i++){
		auto sprite=(Sprite*)dragon_array.at(i);
		Vec2 pos=sprite->getPosition();
		sprite->setPosition(Vec2(pos.x+RandomBetweenAandB(0,10)-5.f,pos.y+5.f));

		if(sprite->getPositionY()>winSize.height){
			dragon_array.eraseObject(sprite);
			removeChild(sprite);
		}
	}
}

bool GuideScene::onTouchBegan(Touch* touch, Event* event)
{
	//playBtnClkSound(2);
	Vec2 pos = convertTouchToNodeSpace(touch);
	if (m_pTap->getBoundingBox().containsPoint(pos))
	{
		touchStart = pos;
		//touchStatus = true;
		moveOldPoint=touchStart;
		initTapPos = m_pTap->getPosition();
		initRingPos = m_pRingLeft->getPosition();
		return true;
	}
	else if(m_pSoundOn->getBoundingBox().containsPoint(pos))
	{
		if(sound_state)
		{
			sound_state=false;
			m_pSoundOn->setVisible(false);
		}
		else
		{
			sound_state=true;
			m_pSoundOn->setVisible(true);
		}
		UserDefault::getInstance()->setBoolForKey("sound_state",sound_state);
		UserDefault::getInstance()->flush();
		return true;
	}
	else if (m_pMusicOn->getBoundingBox().containsPoint(pos))
	{
		if(music_state)
		{
			music_state=false;
			m_pMusicOn->setVisible(false);
			
			SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		}
		else
		{
			music_state=true;
			m_pMusicOn->setVisible(true);
			
			SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		}
		UserDefault::getInstance()->setBoolForKey("music_state",music_state);
		UserDefault::getInstance()->flush();
		return true;
	}
	return false;
	
}

void GuideScene::onTouchMoved(Touch* touch, Event* event)
{
	Vec2 curPos = convertTouchToNodeSpace(touch);
	Vec2 psub;
	Vec2 psubTap;
	Vec2 psubRing;
	if (m_pTap->getBoundingBox().containsPoint(curPos))
	{
		

		fDistance = curPos.distance(moveOldPoint);
		if(fDistance < 20.f * fScaleN)
		{
			psub = touchStart - initTapPos;		// tap init delta
			psubTap = curPos - psub;				// current tap position
			m_pTap->setPosition(psubTap);

			psub = touchStart - initRingPos;		
			psubRing = curPos - psub;
			m_pRingLeft->setPosition(psubRing);
			m_pRingRight->setPosition(psubRing);
			m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
			m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));
			moveOldPoint = curPos;
		}
	}	

}

void GuideScene::onTouchEnded(Touch* touch, Event* event)
{
	Vec2 curPos = convertTouchToNodeSpace(touch);
}

void GuideScene::onRotate(Ref* pSender)
{
	float rotate_angle;
	auto item = dynamic_cast<MenuItemImage*>(pSender);
	if(item == m_pRightButton)
	{
		rotate_angle=10.f;
	}
	else
	{
		rotate_angle=-10.f;
	}

	m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
	m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));
	m_pRingUpTemp->setRotation(m_pRingLeft->getRotation()+rotate_angle);
	m_pRingDownTemp->setRotation(m_pRingLeft->getRotation()+rotate_angle);
	m_pRingLeft->setRotation(m_pRingLeft->getRotation() + rotate_angle);
	m_pRingRight->setRotation(m_pRingRight->getRotation() + rotate_angle);
	if(m_pRingLeft->getRotation() > 45.f)
		m_pRingLeft->setRotation(45.f);
	if(m_pRingLeft->getRotation() < -45.f)
		m_pRingLeft->setRotation(- 45.f);
	if(m_pRingRight->getRotation() >45.f)
		m_pRingRight->setRotation(45.f);
	if(m_pRingRight->getRotation() < -45.f)
		m_pRingRight->setRotation(-45.f);
}

void GuideScene::drawBonus(Vec2 pos){

	Sprite* m_pBonus=Sprite::create("common/bonus.png");
	m_pBonus->setPosition(pos);
	m_pBonus->runAction(MoveBy::create(1.f,Vec2(0.f,50.f)));
	m_pBonus->runAction(Sequence::create(FadeIn::create(1.0f), CallFunc::create(CC_CALLBACK_0(GuideScene::removeBonus,this,m_pBonus)), NULL));
	addChild(m_pBonus,6);
}

void GuideScene::removeBonus(Node* sender){

	removeChild(sender);
	m_nScore+=10;
	char buf[64];
	sprintf(buf,"%d",m_nScore);
	lb_score->setString(buf);
}

void GuideScene::nextChapter(Node* sender){

	Director *pDirector = Director::getInstance();
	Scene *pScene = MainGameScene::createScene();
	pDirector->replaceScene(TransitionFade::create(1.5f, pScene));
}

void GuideScene::showGameOver(bool bWin){

	if(bWin)
		m_pGameOverBoard = Sprite::create("game_over/broad_youpassed.png");
	else{
		m_pGameOverBoard = Sprite::create("game_over/broad_gameover.png");
	}
	win_or_lose=bWin;
	CommonUtiles::setAdjustScale(m_pGameOverBoard);
	m_pGameOverBoard->setScale(m_pGameOverBoard->getScale() * 0.5f);
	m_pGameOverBoard->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	addChild(m_pGameOverBoard,8);

	Size board_size=m_pGameOverBoard->getContentSize();

	/////////////share on
	Sprite* m_pShareOn = Sprite::create("button/share-on.png");
	m_pShareOn->setPosition(Vec2(board_size.width * 0.25f, board_size.height * 0.25f));
	m_pShareOn->setScale(m_pShareOn->getScale()*0.5f);
	m_pGameOverBoard->addChild(m_pShareOn, 2);

	//score label
	auto lb_score= Label::createWithTTF("Your score",fontList[3],60, Size(winSize.width * 0.7f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_score->setVerticalAlignment(TextVAlignment::CENTER);
	lb_score->setPosition( Vec2(board_size.width*0.35f, board_size.height*0.6f));
	lb_score->setColor(Color3B(255,255,255));
	m_pGameOverBoard->addChild(lb_score,1);

	auto lb_highscore= Label::createWithTTF("high score",fontList[3],60, Size(winSize.width * 0.7f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_highscore->setVerticalAlignment(TextVAlignment::CENTER);
	lb_highscore->setPosition( Vec2(board_size.width*0.35f, board_size.height*0.4f));
	lb_highscore->setColor(Color3B(255,255,255));
	m_pGameOverBoard->addChild(lb_highscore,1);

	char buf[30];
	sprintf(buf,"%d",m_nScore);
	auto your_score= Label::createWithTTF(buf,fontList[3],60, Size(winSize.width * 0.3f, winSize.height * 0.35f), TextHAlignment::CENTER);
	your_score->setVerticalAlignment(TextVAlignment::CENTER);
	your_score->setPosition( Vec2(board_size.width*0.7f, board_size.height*0.6f));
	your_score->setColor(Color3B(0,62,98));
	m_pGameOverBoard->addChild(your_score,1);

	sprintf(buf,"%d",UserDefault::getInstance()->getIntegerForKey("high_score",0));
	auto high_score= Label::createWithTTF(buf,fontList[3],60, Size(winSize.width * 0.3f, winSize.height * 0.35f), TextHAlignment::CENTER);
	high_score->setVerticalAlignment(TextVAlignment::CENTER);
	high_score->setPosition( Vec2(board_size.width*0.7f, board_size.height*0.4f));
	high_score->setColor(Color3B(0,62,98));
	m_pGameOverBoard->addChild(high_score,1);

	//////button
	Sprite* pSprite1 = Sprite::create("game_over/stage_button.png");
	Sprite* pSprite2 = Sprite::create("game_over/stage_button.png");
	pSprite2->setOpacity(150);
	MenuItemSprite* m_pGMStageItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pGMStageItem->setScale(m_pGMStageItem->getScale() * 0.8f);
	m_pGMStageItem->setPosition(Vec2(board_size.width * 0.2f, board_size.height * 0.05f));
	m_pGMStageItem->setTag(enGameOverMenu_Stage);

	pSprite1 = Sprite::create("game_over/retry_button.png");
	pSprite2 = Sprite::create("game_over/retry_button.png");
	pSprite2->setOpacity(150);
	MenuItemSprite* m_pGMRetryItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pGMRetryItem->setScale(m_pGMRetryItem->getScale() * 0.8f);
	m_pGMRetryItem->setPosition(Vec2(board_size.width * 0.8f, board_size.height * 0.05f));
	m_pGMRetryItem->setTag(enGameOverMenu_Retry);


	pSprite1 = Sprite::create("game_over/next_button.png");
	pSprite2 = Sprite::create("game_over/next_button.png");
	pSprite2->setOpacity(150);
	MenuItemSprite* m_pGMNextItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pGMNextItem->setScale(m_pGMNextItem->getScale() * 0.8f);
	m_pGMNextItem->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.05f));
	m_pGMNextItem->setTag(enGameOverMenu_Next);

	// facebook button
	pSprite1 = Sprite::create("button/facebook.png");
	pSprite2 = Sprite::create("button/facebook.png");
	pSprite2->setOpacity(150);
	MenuItemSprite* m_pGMFacebookItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pGMFacebookItem->setScale(m_pGMFacebookItem->getScale() * 0.7f);
	m_pGMFacebookItem->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.25f));
	m_pGMFacebookItem->setTag(enGameOverMenu_Facebook);

	// twitter button
	pSprite1 = Sprite::create("button/twitter.png");
	pSprite2 = Sprite::create("button/twitter.png");
	pSprite2->setOpacity(150);
	MenuItemSprite* m_pGMTwitterItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pGMTwitterItem->setScale(m_pGMTwitterItem->getScale() * 0.7f);
	m_pGMTwitterItem->setPosition(Vec2(board_size.width * 0.65f, board_size.height * 0.25f));
	m_pGMTwitterItem->setTag(enGameOverMenu_Twitter);

	// google button
	pSprite1 = Sprite::create("button/google.png");
	pSprite2 = Sprite::create("button/google.png");
	pSprite2->setOpacity(150);
	MenuItemSprite* m_pGMGoogleItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(GuideScene::backCallBack, this));
	m_pGMGoogleItem->setScale(m_pGMGoogleItem->getScale() * 0.7f);
	m_pGMGoogleItem->setPosition(Vec2(board_size.width * 0.8f, board_size.height * 0.25f));
	m_pGMGoogleItem->setTag(enGameOverMenu_Google);

	if(bWin)
		pGameOverMenu = Menu::create(m_pGMStageItem,m_pGMRetryItem,m_pGMNextItem,m_pGMFacebookItem,m_pGMTwitterItem, NULL );
	else
		pGameOverMenu = Menu::create(m_pGMStageItem,m_pGMRetryItem,m_pGMFacebookItem,m_pGMTwitterItem, NULL );
	pGameOverMenu->setPosition(Vec2::ZERO);
	m_pGameOverBoard->addChild(pGameOverMenu, 1);
}

void GuideScene::backCallBack(Ref* pSender)
{
	
}