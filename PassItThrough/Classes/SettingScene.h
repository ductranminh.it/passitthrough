
#ifndef __SETTING_SCENE_H__
#define __SETTING_SCENE_H__

#include "common.h"

class  SettingScene: public cocos2d::Scene
{
public:
	SettingScene();
	virtual ~SettingScene();
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();
	static SettingScene* create();

	// implement the "static node()" method manually
	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);
	void DrawMusicVolume(int num);
	void backCallBack(Ref* pSender);
	
	void initBackground();
	//CREATE_FUNC(SettingScene);
	
public:
	float zoomx;
	float zoomy;
	Menu* m_pMainMenu;
//	bool soundon;
	bool instruction_state;
	Sprite* sprite;
	bool BackgroundMusicOn;
	bool SoundOn;
	Vec2 touchStart;
public:
	Sprite *setting_background;
	Sprite* sound_tap;
	Sprite* sound_bar;
	Sprite* music_bar;
	bool touch_status;


};



#endif  // __SETTING_SCENE_H__