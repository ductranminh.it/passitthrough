/************************************************************************
*  RankingScene.cpp
*
*
*  Created by udinsoft
*  Copyright udinsoft 2019-5-15
*
************************************************************************/

#include "RankingScene.h"
//#include "SelectLevelScene.h"

#include "MainGameScene.h"
//#include "GameContinueScene.h"
#include "FirstScene.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>

#endif

using namespace cocos2d;
using namespace CocosDenshion;

enum enRankingSceneMenu{
	enClose             = 1,
	enMenu
};

Scene* RankingScene::createScene(){ 

	return RankingScene::create();
}

// on "init" you need to initialize your instance
bool RankingScene::init(){

	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}

	initBackground();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(RankingScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(RankingScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(RankingScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	return true;
}


void RankingScene::initBackground()
{
	//background
	setting_background = Sprite::create("ranking/broad.png");
	CommonUtiles::setAdjustScale(setting_background);
 	setting_background->setPosition( Vec2(winSize.width *0.5f, winSize.height * 0.5f));
	setting_background->setScale(setting_background->getScale()*0.3f);
	addChild(setting_background,2);
	float m_nWidth=setting_background->getContentSize().width;
	float m_nHeight=setting_background->getContentSize().height;

	getValue();

	char buf[32];

	for(int i=0;i<5;i++)
	{
		sprintf(buf,"%d",i+1);
		rank_num[i] = Label::createWithTTF(buf, fontList[3], 60, Size(winSize.width * 0.6f, winSize.height * 0.35f), TextHAlignment::CENTER);
		rank_num[i]->setVerticalAlignment(TextVAlignment::CENTER);
		rank_num[i]->setPosition( Vec2(m_nWidth*0.22f, m_nHeight*(0.66f-0.1f*i)));
		rank_num[i]->setColor(Color3B(254,201,33));
		setting_background->addChild(rank_num[i],1);

		sprintf(buf,"%d",high_score[i]);
		rank_score[i] = Label::createWithTTF(buf, fontList[3], 60, Size(winSize.width * 0.6f, winSize.height * 0.35f), TextHAlignment::CENTER);
		rank_score[i]->setVerticalAlignment(TextVAlignment::CENTER);
		rank_score[i]->setPosition( Vec2(m_nWidth*0.4f,m_nHeight*(0.66f-0.1f*i)));
		rank_score[i]->setColor(Color3B(254,201,33));
		setting_background->addChild(rank_score[i],1);

		
		rank_date[i] = Label::createWithTTF(high_time[i], fontList[3],60,Size(winSize.width * 0.6f, winSize.height * 0.35f), TextHAlignment::CENTER);
		rank_date[i]->setVerticalAlignment(TextVAlignment::CENTER);
		rank_date[i]->setPosition( Vec2(m_nWidth*0.72f, m_nHeight*(0.66f-0.1f*i)));
		rank_date[i]->setColor(Color3B(254,201,33));
		setting_background->addChild(rank_date[i],1);
	}

	//cancel
	MenuItemImage* btn_cancel = MenuItemImage::create("option_screen/close.png", "option_screen/close.png", CC_CALLBACK_1(RankingScene::backCallBack, this));
	btn_cancel->setTag(enClose);
	btn_cancel->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.1f));
	
	m_pMainMenu=Menu::create(btn_cancel,NULL);	
	m_pMainMenu->setPosition(Vec2::ZERO);
	m_pMainMenu->setTag(enMenu);
	setting_background->addChild(m_pMainMenu,3);

	touch_status=false;
	instruction_state=false;
	
}

void RankingScene::getValue()
{
	high_score[0]=UserDefault::getInstance()->getIntegerForKey("highscore", 0);
	high_score[1]=UserDefault::getInstance()->getIntegerForKey("secondscore",0);
	high_score[2]=UserDefault::getInstance()->getIntegerForKey("thirdscore",0);
	high_score[3]=UserDefault::getInstance()->getIntegerForKey("forthscore",0);
	high_score[4]=UserDefault::getInstance()->getIntegerForKey("fifthscore",0);

	high_time[0]=UserDefault::getInstance()->getStringForKey("hightime", "");
	high_time[1]=UserDefault::getInstance()->getStringForKey("secondtime","");
	high_time[2]=UserDefault::getInstance()->getStringForKey("thirdtime","");
	high_time[3]=UserDefault::getInstance()->getStringForKey("forthtime","");
	high_time[4]=UserDefault::getInstance()->getStringForKey("fifthtime","");
}

void RankingScene::backCallBack(Ref* pSender){
	
	switch(((MenuItem *)pSender)->getTag())
	{
	
	case enClose:			
		{
			
			this->getParent()->resume();

			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();
			//this->getParent()->removeChild(setting_background);

			break;
		}

	}
}

bool RankingScene::onTouchBegan(Touch* touch, Event* event){

	touchStart = setting_background->convertTouchToNodeSpace(touch);
	return false;
}

void RankingScene::onTouchMoved(Touch* touch, Event* event){

	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

void RankingScene::onTouchEnded(Touch* touch, Event* event){

	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

