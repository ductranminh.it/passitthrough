/************************************************************************
*  common.h																
*																		
*																		
*  Created by Lin dao min									
*  Copyright Lin 2014-4-5											
*																		
************************************************************************/



#ifndef __COMMON_H__
#define __COMMON_H__

#define NORMALWIDTH			800
#define NORMALHEIGHT		480
//#define NORMALWIDTH		1024
//#define NORMALHEIGHT		768

#define WIRE_MOVE_TIME_INTERVAL     0.2
#define WIRE_MOVE_STEP              2


#define RAD_PI							3.141592

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

#define TAG_RING						100
#define TAG_WIRE						1000


#define LEVEL_KIND			3
#define CHAPTER_MAX			20
#define REMOVE_ADS			"Remove ads"
#define UNLOCK_ALL_LEVELS	"Unlock all game levels"
#define EVENT_BUY_SUCCESS	"BUY_SUCCESS"
#define EVENT_BUY_CANCEL	"BUY_CANCEL"
#define EVENT_BUY_FAILED	"BUY_FAILED"
#define EVENT_RT_SUCCESS 	"RESTORE_SUCCESS"

#define	MP3_BUTTON_HIT		"sound/button_hit.mp3"

#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))

#define FACEBOOK_SHARE_URL  "https://www.facebook.com/sharer/sharer.php?u="
#if( CC_TARGET_PLATFORM == CC_PLATFORM_IOS )
#define    STORE_URL        "https://apps.apple.com/us/app/pass-it-through/id1466024767"
#else
#define    STORE_URL        "http://bit.ly/2K41Yhl"
#endif

#define str_concat(first, second) first second
extern std::string fontList[];
extern std::string high_time[5];

using namespace cocos2d;
using namespace CocosDenshion;

extern float fScaleN;
extern float fScaleX;
extern float fScaleY;
extern Size winSize;
extern Size winSizeInPixel;
extern float fScaleRetina;

//extern int gPlayingType;		// 0: puzzle , 1 : arcade
extern int gLevelPageNum;

extern bool first_install;
extern bool nothanks_flag;

//extern bool music;
//extern bool sound_effect;
extern bool share_facebook;

extern int m_nMainSceneAppearNumber;

extern bool music_state;
extern bool sound_state;
extern bool sound_state_changed;
extern bool music_state_changed;
extern bool first_booting;
extern bool rate_booting;
extern bool ad_booting;
extern bool remove_ad;
extern bool unlock_all;
extern bool is_iap_enabled;

extern char* page_index_string[4];

extern int curve_num[LEVEL_KIND][CHAPTER_MAX];

extern int curve_difficulty[CHAPTER_MAX];
extern bool level_or_chapter;
extern bool menu_halt;

extern float sound_volume;
extern float music_volume;

extern int high_score[5];

//extern int selected_level;
//extern int selected_chapter;

//extern CCDictionary* strongHoldInfo;

class CommonUtiles
{
public:
	static void setNormalScale(Node* obj);
	static void setAdjustScale(Node* obj);
    
};

#endif 
