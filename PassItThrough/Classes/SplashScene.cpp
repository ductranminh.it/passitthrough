/************************************************************************
*  SplashScene.cpp														
*																		
*																		
*  Created by udinsoft
*  Copyright udinsoft 2019 - 5 - 14											
*																		
************************************************************************/

#include "SplashScene.h"
#include "FirstScene.h"
#include "MainGameScene.h"
#include "adsManager.h"
#include "GuideScene.h"

using namespace cocos2d;
using namespace CocosDenshion;

Scene* SplashScene::createScene()
{
	return SplashScene::create();
}

// on "init" you need to initialize your instance
bool SplashScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	AdsManager::getInstance()->hideAdMob();


	Sprite* pBg = NULL;
	int nWidth = winSize.width;
	int nHeight = winSize.height;

	nothanks_flag = UserDefault::getInstance()->getBoolForKey("nothanks_flag", false);

	first_install = UserDefault::getInstance()->getBoolForKey("first_install", true);
	remove_ad = UserDefault::getInstance()->getBoolForKey("remove_ad", false);
	unlock_all = UserDefault::getInstance()->getBoolForKey("unlock_all", false);

	//remove_ad = false;
	//UserDefault::getInstance()->setBoolForKey("remove_ad", false);


	//UserDefault::getInstance()->setIntegerForKey("game_open_times", 0);
	//show ads
	int game_open_times = UserDefault::getInstance()->getIntegerForKey("game_open_times", 0);
    //UserDefault::getInstance()->setIntegerForKey("game_open_times", 0);

	if (game_open_times < 3)
	{
		game_open_times++;
		if (game_open_times % 2 == 0)    ///show ad popup when app is running 2 times
		{
			//game_open_times = 0;
			ad_booting = true;   //true then show popup remind remove ads
		}
		else
		{
            ad_booting = false;  ///else no display
		}

        if (game_open_times % 3 == 0)    ///show ad when app is running 3 times
        {
            //game_open_times = 0;
            first_booting = true;   //true then show banner init
        }
        else
        {
            first_booting = false;  ///else no display
        }

		rate_booting = false;
		log("game_open_times=%d", game_open_times);
		UserDefault::getInstance()->setIntegerForKey("game_open_times", game_open_times);
		UserDefault::getInstance()->flush();
	}
	else
	{
		game_open_times++;
		//show rate
		if (game_open_times == 3) {

			rate_booting = true;   //true then show rate
			log("game_open_times=%d", game_open_times);
			UserDefault::getInstance()->setIntegerForKey("game_open_times", game_open_times);
			UserDefault::getInstance()->flush();
		}
		else
		{
			rate_booting = false;  ///else no display
		}
		ad_booting = false;
		first_booting = true;
	}



	if (nWidth % 1024 == 0 && nHeight % 768 == 0)
	{
		pBg = Sprite::create("splash screen/1024.png");


	}
	else if (nWidth % 1280 == 0 && nHeight % 800 == 0)
	{
		pBg = Sprite::create("splash screen/1280.png");

	}
	else if (nWidth % 800 == 0 && nHeight % 480 == 0)
	{
		pBg = Sprite::create("splash screen/800.png");

	}
	else if (nWidth % 480 == 0 && nHeight % 320 == 0)
	{
		pBg = Sprite::create("splash screen/960.png");

	}
	else if (nWidth % 568 == 0 && nHeight % 320 == 0)
	{
		pBg = Sprite::create("splash screen/1136.png");

	}
	else
	{
		pBg = Sprite::create("splash screen/1280.png");

	}

	pBg->setScaleX(winSize.width / pBg->getContentSize().width * fScaleRetina);
	pBg->setScaleY(winSize.height / pBg->getContentSize().height * fScaleRetina);
	pBg->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	addChild(pBg);


	Sprite* m_pPad = Sprite::create("splash screen/loading.png");
	CommonUtiles::setAdjustScale(m_pPad);
	m_pPad->setScale(m_pPad->getScale()*0.5f);
	m_pPad->setPosition(Vec2(winSize.width *0.5f, winSize.height * 0.4f));
	addChild(m_pPad, 2);

	m_TemperatureProgressBar = ProgressTimer::create(Sprite::create("splash screen/load.png"));
	CommonUtiles::setAdjustScale(m_TemperatureProgressBar);
	m_TemperatureProgressBar->setScale(m_TemperatureProgressBar->getScale()*0.5f);

	m_TemperatureProgressBar->setPosition(Vec2(winSize.width *0.5f, winSize.height * 0.4f));
	addChild(m_TemperatureProgressBar, 2);
	m_TemperatureProgressBar->setType(ProgressTimer::Type::BAR);
	m_TemperatureProgressBar->setMidpoint(Vec2(0.f, 0.5f));
	m_TemperatureProgressBar->setBarChangeRate(Vec2(1.f, 0.f));		// from left to right
	m_TemperatureProgressBar->setPercentage(0);

	bar_length = 0;
	schedule(schedule_selector(SplashScene::onTimer), 0.1f);

	this->runAction(Sequence::create(DelayTime::create(2.5f), CallFunc::create(CC_CALLBACK_0(SplashScene::onLoadingEnd,this,this)), NULL));

	return true;
}

void SplashScene::onTimer(float dt)
{
	bar_length+=5;
	if(bar_length>=100)
		unschedule(schedule_selector(SplashScene::onTimer));
	m_TemperatureProgressBar->setPercentage(bar_length);
}

void SplashScene::onLoadingEnd(Node *node)
{
	Director *pDirector = Director::getInstance();
	auto pScene = FirstScene::createScene();

	pDirector->replaceScene(TransitionFade::create(0.3f, pScene));
}

