
#ifndef __ABOUT_SCENE_H__
#define __ABOUT_SCENE_H__

#include "common.h"

class  AboutScene: public cocos2d::Scene
{
public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone

	virtual bool init();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();

	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);
	
	void backCallBack(Ref* pSender);
	
	void initBackground();

	CREATE_FUNC(AboutScene);
	
	
public:
	float zoomx;
	float zoomy;
	Menu* m_pMainMenu;
//	bool soundon;
	bool instruction_state;
	Sprite* sprite;
	bool BackgroundMusicOn;
	bool SoundOn;
	Vec2 touchStart;
public:
	Sprite *setting_background;
	
	bool touch_status;


};



#endif  // __ABOUT_SCENE_H__