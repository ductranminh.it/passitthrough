/************************************************************************
*  MainGameScene.h														
*																		
*																		
*  Created by Lin dao min									
*  Copyright Lin 2014-4-5											
*																		
************************************************************************/

#ifndef __MAIN_GAME_SCENE_H__
#define __MAIN_GAME_SCENE_H__

#include "common.h"
#include <iostream>
#include <stdlib.h>
#include <string>

//#include "atlbase.h"
//#include "atlstr.h"
//#include "comutil.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
#define SDKBOX_ENABLED 1
#ifdef SDKBOX_ENABLED
#include "PluginShare/PluginShare.h"
#endif
class MainGameScene: public cocos2d::Scene, public sdkbox::ShareListener
#else
    class MainGameScene: public cocos2d::Scene
#endif
{
public: 
	MainGameScene();
	virtual ~MainGameScene();
	virtual bool init();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();

	//virtual bool onTouchBegan(Touch* touch, Event* event);
	//virtual void onTouchMoved(Touch* touch, Event* event);
	//virtual void onTouchEnded(Touch* touch, Event* event);
    void onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event);
    void onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event);
    void onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event);
    void onTouchesCancelled(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event);

	static MainGameScene* create();

	virtual void didAccelerate(Acceleration* pAccelerationValue);

	virtual void keyBackClicked();

	void backCallBack(Ref* pSender);


///pause layer button method 
	void onReplayItem(Ref* pSender);
	void onResume(Ref* pSender);
	void onPauseSetting(Ref* pSender);
	void onPauseStage(Ref* pSender);
///main game scene button method
	void onSetting(Ref* pSender);
	void onPause(Ref* pSender);
	void onRotate(Ref* pSender);
	void onReturnSelectLevel(Ref* pSender);
	void onReturnHome(Ref* pSender);

	void update(float dt);
	void onTimer(float dt);
	void initWire();
	void WireMoveAnimation(float dt);
	int RandomBetweenAandB(int a,int b);
	void DelayMoveAnimation();
	void DragonGeneration(); 
	void StoneAppear(float dt);
	void DragonAppear(float dt);

	int vibrate();

	void drawBonus(Vec2 pos);       // score 10+ animation
	void removeBonus(Node* sender); 
	void nextChapter(Node* sender);
	void OutDragonGeneration(float dt);
	void VibrateDevice(float dt);

	void showGameOver(bool bWin);
    void showResult();
	void setValue();

	void execute(EventCustom* event);

	PointArray *m_pArray;

	//CREATE_FUNC(MainGameScene);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    void afterCaptureScreen(bool yes, const std::string &outputFilename);

#endif

public:
	bool bIsCollision;
	Sprite* m_pRingRight;
	Sprite* m_pRingLeft;
	Sprite* m_pRingUp;
	Sprite* m_pRingDown;
	Sprite* m_pRingUpTemp;
	Sprite* m_pRingDownTemp;
	Sprite* angle_pad;

	Sprite* m_pWire[30];
	
	Sprite* m_pTap;
	Sprite* m_pLoseMessage;
	Sprite* m_pWinMessage;
	Sprite* m_pSignBoard;

	Label* lb_score;

	cocos2d::Vector<Sprite*> dragon_array;
	Sprite* m_pStone;

/////main game 
	MenuItemImage* m_pReplayItem;
	MenuItemImage* m_pReturnHome;
	MenuItemImage* m_pReturnSelectlevel;
	MenuItemImage* m_pRightButton;
	MenuItemImage* m_pLeftButton;
	MenuItemImage* m_pPauseItem;
////pause layer
	Sprite* m_pPauseBoard;
	Sprite* m_pPauseMask;
	MenuItemImage* m_pResume;
	MenuItemImage* m_pRestart;
	MenuItemImage* m_pOption;
	MenuItemImage* m_pStage;
	Menu* pPauseMenu;
	////game over layer
	Sprite* m_pGameOverBoard;
    Sprite* unlock_background;
	Menu* pGameOverMenu;

	Sprite* m_pMusicOn;
	Sprite* m_pMusicOff;
	Sprite* m_pSoundOn;
	Sprite* m_pSoundOff;

	Menu * pMenu;

	Vec2 curPoint;
	Vec2 touchStart;
	Vec2 moveOldPoint;
	float fDistance;
	bool touchStatus;
	Vec2 initTapPos;
	Vec2 initRingPos;
	Vec2 initRingUpPos;
	Vec2 initRingDownPos;
	int touch_kind;
	int pattern_width;
	int pattern_height;
	int num;   //////////////////////////////////////////////////////////////////////////curve num in selected chapter

	bool ring_move_start;  ///////when user moves the ring, this flag is true


	float dis;
	float dis_temp;

	float move_len;   //wire move length
	void showUnlockAllLevels();

public:


	int	m_nSelectedChapter;
	int m_nSelectedLevel;
	int m_nTotalCompleteChapter;
	int m_nTotalSelectChapter;

	int m_nScore;
	int dragon_num;
	int m_nVibrateNum;    //wire vibrate height determine value 
	int vibrate_height;  //wire vibrate height
	float endPointX;    // wire end point
	float startPointX;    // wire start point
	float displayEndPointX;  
	
	float dragon_width;
	int outDragonNum;

	int index;
	int stone_index;
	bool stone_enable;
	bool win_or_lose;
private:
    bool result = false;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)

    std::string _captureFilename;

    virtual void onShareState(const sdkbox::SocialShareResponse& response);

#endif

};

#endif  // __MAIN_GAME_SCENE_H__
