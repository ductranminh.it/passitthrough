/************************************************************************
*  SelectLevelScene.cpp													
*																		
*																		
*  Created by Lin dao min											
*  Copyright Lin 2014-4-11											
*																		
************************************************************************/

#include "SelectLevelScene.h"

#include "MainGameScene.h"
#include "FirstScene.h"
#include "CloseScene.h"
#include "adsManager.h"
#include "GuideScene.h"

using namespace cocos2d;
using namespace CocosDenshion;


enum enSelectLevelSceneMenu{
	enSelectLevelSceneMenu_Home				= 100,
	enSelectLevelSceneMenu_Prev				= 101,
	enSelectLevelSceneMenu_Next				= 102,
	enSelectLevelSceneMenu_Unlock			= 103,

	enSelectLevelSceneMenu_Chapter0			= 200,
	enSelectLevelSceneMenu_ChapterNum0		= 400,
	enSelectLevelSceneMenu_ChapterLock0		= 600,
};

enum enLevel_Menu{
	enLevel_Easy				= 10,
	enLevel_Medium				= 11,
	enLevel_Hard				= 12,
};


Scene* SelectLevelScene::createScene(){
	return SelectLevelScene::create();
}

// on "init" you need to initialize your instance
bool SelectLevelScene::init(){

	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	AdsManager::getInstance()->hideAdMob();

	//unlock_all = false;

	// background
	auto pBg = Sprite::create("common/level_bg.png");
	Size backSize = pBg->getContentSize();
	pBg->setScaleY(winSize.height / pBg->getContentSize().height * fScaleRetina);
	pBg->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	pBg->setScaleX(winSize.width / pBg->getContentSize().width * fScaleRetina);
	addChild(pBg, 0);

	// left background mask
	auto temp = Sprite::create("common/level_bg.png", Rect(0, 0, backSize.width * 0.08f, backSize.height));
	temp->setScaleX(winSize.width / pBg->getContentSize().width * fScaleRetina);
	temp->setScaleY(winSize.height / pBg->getContentSize().height * fScaleRetina);
	temp->setAnchorPoint(Vec2(0, 0.5f));
	temp->setPosition(Vec2(0, winSize.height * 0.5f));
	addChild(temp, 3);

	// right background mask
	temp = Sprite::create("common/level_bg.png", Rect(backSize.width * 0.92f, 0, backSize.width * 0.08f, backSize.height));
	temp->setScaleX(winSize.width / pBg->getContentSize().width * fScaleRetina);
	temp->setScaleY(winSize.height / pBg->getContentSize().height * fScaleRetina);
	temp->setAnchorPoint(Vec2(1, 0.5f));
	temp->setPosition(Vec2(winSize.width, winSize.height * 0.5f));
	addChild(temp, 3);

	// "Levels" title
	if (level_or_chapter)
		initLevelMenu();
	else
		onDrawChapters();

    auto _listener1 = EventListenerCustom::create(EVENT_BUY_SUCCESS, std::bind(&SelectLevelScene::execute, this, std::placeholders::_1));
    if(_eventDispatcher->hasEventListener(EVENT_BUY_SUCCESS)){
        _eventDispatcher->removeCustomEventListeners(EVENT_BUY_SUCCESS);
    }
    _eventDispatcher->addEventListenerWithFixedPriority(_listener1, 1);

	return true;
}

void SelectLevelScene::execute(EventCustom* event){
	std::string nameEvent = event->getEventName().c_str();
	if (nameEvent == EVENT_BUY_SUCCESS) {
        Director *pDirector = Director::getInstance();
        auto pScene = SelectLevelScene::createScene();
        pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
	}
}


void SelectLevelScene::keyBackClicked(){
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
	pMenu->setEnabled(false);
	Director *pDirector = Director::getInstance();
	Scene *pScene = FirstScene::createScene();
	pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
}

void SelectLevelScene::onTimer(float dt){
	if(!menu_halt){
		pMenu->setEnabled(true);
		Director::getInstance()->getEventDispatcher()->resumeEventListenersForTarget(this);
		unschedule(schedule_selector(SelectLevelScene::onTimer));
	}
}

void SelectLevelScene::initLevelMenu(){

	MenuItemImage* btnEasy = MenuItemImage::create("button/btn_classic.png", "button/bts_classic.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
	CommonUtiles::setAdjustScale(btnEasy);
	btnEasy->setTag(enLevel_Easy);
	btnEasy->setPosition(Vec2(winSize.width * 0.26f, winSize.height * 0.525f));	// 125 380

	// arcade button
	MenuItemImage* btnMedium = MenuItemImage::create("button/btn_arcade.png", "button/bts_arcade.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
	CommonUtiles::setAdjustScale(btnMedium);
	btnMedium->setTag(enLevel_Medium);
	btnMedium->setPosition(Vec2(winSize.width * 0.8f, winSize.height * 0.43f));	// 384 456

	// text button
	MenuItemImage* btnHard = MenuItemImage::create("button/btn_text.png", "button/bts_text.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
	CommonUtiles::setAdjustScale(btnHard);
	btnHard->setTag(enLevel_Hard);
	btnHard->setPosition(Vec2(winSize.width * 0.28f, winSize.height * 0.145f));	// 145 683
	Menu * pMenu = Menu::create(btnEasy,btnMedium,btnHard, NULL );
	pMenu->setPosition(Vec2::ZERO );
	addChild(pMenu,2);
}

void SelectLevelScene::onDrawChapters(){
	m_nMaxChapter=CHAPTER_MAX*3;
	m_nTotalCompleteChapter=UserDefault::getInstance()->getIntegerForKey("total_complete_chapter", 0);

	m_nCompleteLevel=m_nTotalCompleteChapter/CHAPTER_MAX;
	m_nCompleteChapter=m_nTotalCompleteChapter%CHAPTER_MAX;
	
////level title and board
	Sprite* temp = Sprite::create("level screen/broad.png");
	CommonUtiles::setAdjustScale(temp);
	temp->setScale(temp->getScale()*0.6f);
	temp->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.9f));
	addChild(temp, 2);

	auto m_pTitleLabel = Label::createWithTTF("Level", fontList[3], 60 * fScaleN, Size(winSize.width * 0.8f, winSize.height * 0.35f), TextHAlignment::CENTER);
	m_pTitleLabel->setVerticalAlignment(TextVAlignment::CENTER);
	m_pTitleLabel->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.9f));
	m_pTitleLabel->setColor(Color3B(254,201,33));
	addChild(m_pTitleLabel, 2);

	// home button
	m_pHomeItem = MenuItemImage::create("button/icon-home.png", "button/icon-home.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pHomeItem);
	m_pHomeItem->setScale(m_pHomeItem->getScale() * 0.7f);
	m_pHomeItem->setTag(enSelectLevelSceneMenu_Home);
	m_pHomeItem->setPosition(Vec2(winSize.width * 0.1f, winSize.height * 0.9f));

	// prev button
	m_pPrevItem = MenuItemImage::create("level screen/left.png", "level screen/left.png","level screen/left-gray.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pPrevItem);
	m_pPrevItem->setScale(m_pPrevItem->getScale() * 0.4f);
	m_pPrevItem->setTag(enSelectLevelSceneMenu_Prev);
	m_pPrevItem->setPosition(Vec2(winSize.width * 0.06f, winSize.height * 0.48f));

	// next button
	m_pNextItem = MenuItemImage::create("level screen/right.png", "level screen/right.png","level screen/right-gray.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
	CommonUtiles::setAdjustScale(m_pNextItem);
	m_pNextItem->setScale(m_pNextItem->getScale() * 0.4f);
	m_pNextItem->setTag(enSelectLevelSceneMenu_Next);
	m_pNextItem->setPosition(Vec2(winSize.width * 0.94f, winSize.height * 0.48f));

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
	if (is_iap_enabled){
		if (!unlock_all) {
			// unlock all
			m_pUnlockItem = MenuItemImage::create("button/unlock_all.png", "button/unlock_all.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
			CommonUtiles::setAdjustScale(m_pUnlockItem);
			m_pUnlockItem->setScale(m_pUnlockItem->getScale() * 0.5f);
			m_pUnlockItem->setTag(enSelectLevelSceneMenu_Unlock);
			m_pUnlockItem->setPosition(Vec2(winSize.width * 0.85f, winSize.height * 0.9f));

			pMenu = Menu::create(m_pHomeItem, m_pPrevItem, m_pNextItem, m_pUnlockItem, NULL);
		}else
			pMenu = Menu::create(m_pHomeItem, m_pPrevItem, m_pNextItem, NULL);
	}else 
		pMenu = Menu::create(m_pHomeItem, m_pPrevItem, m_pNextItem, NULL);
#else
        pMenu = Menu::create(m_pHomeItem, m_pPrevItem, m_pNextItem, NULL);
#endif
	

	pMenu->setPosition(Vec2::ZERO );
	addChild(pMenu, 5);

	for(int i=0;i<4;i++){
		page_index[i]=Sprite::create("level screen/small-button.png");
		CommonUtiles::setAdjustScale(page_index[i]);
		page_index[i]->setScale(page_index[i]->getScale()*0.5f);
		page_index[i]->setPosition(Vec2(winSize.width*(0.42f+0.05f*i),winSize.height*0.05f));
		addChild(page_index[i],3);
	}

	page_index_enable=Sprite::create("level screen/small-button-click.png");
	CommonUtiles::setAdjustScale(page_index_enable);
	page_index_enable->setScale(page_index_enable->getScale()*0.5f);
	page_index_enable->setPosition(Vec2(winSize.width*0.42f,winSize.height*0.05f));
	addChild(page_index_enable,3);

	lbPageNum = Label::createWithTTF(page_index_string[0], fontList[3], 30 * fScaleN, Size(winSize.width * 0.3f, winSize.height * 0.15f), TextHAlignment::CENTER);
	lbPageNum->setVerticalAlignment(TextVAlignment::CENTER);
	lbPageNum->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.1f));
	lbPageNum->setColor(Color3B(255,255,255));
	addChild(lbPageNum, 2);

	gLevelPageNum =0;
	m_nLastPageNum = (m_nMaxChapter - 1) / 15;
	if(gLevelPageNum == 0){
		m_pNextItem->setEnabled(true);
		m_pPrevItem->setEnabled(false);
	}
	else if(gLevelPageNum > 0 && gLevelPageNum < m_nLastPageNum){
		m_pNextItem->setEnabled(true);
		m_pPrevItem->setEnabled(true);
	}
	else if(gLevelPageNum == m_nLastPageNum){
		m_pNextItem->setEnabled(false);
		m_pPrevItem->setEnabled(true);
	}

	for (int i = 0; i < m_nMaxChapter; i++){
		m_pChapterBtn[i] = MenuItemImage::create("level screen/level_button_unlock.png","level screen/level_button_unlock.png","level screen/level_button_lock.png", CC_CALLBACK_1(SelectLevelScene::backCallBack, this));
		CommonUtiles::setAdjustScale(m_pChapterBtn[i]);
		m_pChapterBtn[i]->setScale(m_pChapterBtn[i]->getScale() * 0.4f);
		m_pChapterBtn[i]->setTag(enSelectLevelSceneMenu_Chapter0 + i);

		int nPageNum = i / 15;
		int nRowID = (i % 15) / 5;
		int nColID = (i % 15) % 5;
		m_pChapterBtn[i]->setPosition(Vec2(winSize.width * (nPageNum + 0.16f + 0.17f * nColID), winSize.height * (0.7f - 0.22f * nRowID)));

		Menu *pLevelMenu = Menu::create(m_pChapterBtn[i], NULL);
		pLevelMenu->setPosition(Vec2::ZERO);
		addChild(pLevelMenu, 1);

		if(i <= m_nTotalCompleteChapter){
			m_pChapterBtn[i]->setEnabled(true);

			// level number
			auto m_pNumberLabel = Label::createWithTTF("", fontList[3], 50 * fScaleN, Size(winSize.width * 0.3f, winSize.height * 0.1f), TextHAlignment::CENTER);
			m_pNumberLabel->setVerticalAlignment(TextVAlignment::CENTER);
			m_pNumberLabel->setPosition(m_pChapterBtn[i]->getPosition());
			m_pNumberLabel->setColor(Color3B(255, 255, 255));
			m_pNumberLabel->setTag(enSelectLevelSceneMenu_ChapterNum0 + i);
			addChild(m_pNumberLabel, 2);
			m_pNumberLabel->setString(CCString::createWithFormat("%d", i + 1)->getCString());
		}
		else{
			if(!unlock_all)m_pChapterBtn[i]->setEnabled(false);
			else{
                m_pChapterBtn[i]->setEnabled(true);
                // level number
                auto m_pNumberLabel = Label::createWithTTF("", fontList[3], 50 * fScaleN, Size(winSize.width * 0.3f, winSize.height * 0.1f), TextHAlignment::CENTER);
                m_pNumberLabel->setVerticalAlignment(TextVAlignment::CENTER);
                m_pNumberLabel->setPosition(m_pChapterBtn[i]->getPosition());
                m_pNumberLabel->setColor(Color3B(255, 255, 255));
                m_pNumberLabel->setTag(enSelectLevelSceneMenu_ChapterNum0 + i);
                addChild(m_pNumberLabel, 2);
                m_pNumberLabel->setString(CCString::createWithFormat("%d", i + 1)->getCString());
			}
		}
	}

	if(gLevelPageNum > 0){
		for (int i = 0; i < m_nMaxChapter; i++){
			m_pChapterBtn[i]->setPositionX(m_pChapterBtn[i]->getPositionX() - winSize.width * gLevelPageNum);
			if(getChildByTag(enSelectLevelSceneMenu_ChapterNum0 + i)){
				auto pLabel = (Label*)getChildByTag(enSelectLevelSceneMenu_ChapterNum0 + i);
				pLabel->setPositionX(pLabel->getPositionX() - winSize.width * gLevelPageNum);
			}
			if(getChildByTag(enSelectLevelSceneMenu_ChapterLock0 + i)){
				auto pSprite = (Sprite*)getChildByTag(enSelectLevelSceneMenu_ChapterLock0 + i);
				pSprite->setPositionX(pSprite->getPositionX() - winSize.width * gLevelPageNum);
			}
		}
	}
}

void SelectLevelScene::backCallBack(Ref * pSender){
	
	int nTag = ((MenuItem*)pSender)->getTag();

	switch(nTag){
	case enLevel_Easy:
		((Menu*)((MenuItem*)pSender)->getParent())->setVisible(false);
		m_nSelectedLevel=0;
		UserDefault::getInstance()->setIntegerForKey("select_level", m_nSelectedLevel);
		UserDefault::getInstance()->flush();
		onDrawChapters();
		break;
	case enLevel_Medium:
		((Menu*)((MenuItem*)pSender)->getParent())->setVisible(false);
		m_nSelectedLevel=1;
		UserDefault::getInstance()->setIntegerForKey("select_level", m_nSelectedLevel);
		UserDefault::getInstance()->flush();
		onDrawChapters();
		break;

	case enLevel_Hard:
		((Menu*)((MenuItem*)pSender)->getParent())->setVisible(false);
		m_nSelectedLevel=2;
		UserDefault::getInstance()->setIntegerForKey("select_level", m_nSelectedLevel);
		UserDefault::getInstance()->flush();
		onDrawChapters();
		break;

	case enSelectLevelSceneMenu_Home:
		{
			Director *pDirector = Director::getInstance();
			Scene *pScene = FirstScene::createScene();
			pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
			break;
		}
	case enSelectLevelSceneMenu_Prev:
		{
			for (int i = 0; i < m_nMaxChapter; i++){
				m_pChapterBtn[i]->runAction(Sequence::create(MoveBy::create(1.f, Vec2(winSize.width, 0)), NULL));
				if (getChildByTag(enSelectLevelSceneMenu_ChapterNum0 + i)){
					auto pLevelNumLabel = (Label*)getChildByTag(enSelectLevelSceneMenu_ChapterNum0 + i);
					pLevelNumLabel->runAction(Sequence::create(MoveBy::create(1.f, Vec2(winSize.width, 0)), NULL));
				}
				if (getChildByTag(enSelectLevelSceneMenu_ChapterLock0 + i)){
					auto pLevelLockSprite = (Sprite*)getChildByTag(enSelectLevelSceneMenu_ChapterLock0 + i);
					pLevelLockSprite->runAction(Sequence::create(MoveBy::create(1.f, Vec2(winSize.width, 0)), NULL));
				}
			}
			gLevelPageNum --;
			if(gLevelPageNum < 0) 
				gLevelPageNum = 0;
			if(gLevelPageNum == 0){
				m_pPrevItem->setEnabled(false);
				m_pNextItem->setEnabled(true);
			}
			else{
				m_pPrevItem->setEnabled(true);
				m_pNextItem->setEnabled(true);
			}
			page_index_enable->setPositionX(page_index[gLevelPageNum]->getPositionX());
			lbPageNum->setString(page_index_string[gLevelPageNum]);
			break;
		}
	case enSelectLevelSceneMenu_Next:
		{
			for (int i = 0; i < m_nMaxChapter; i++){
				m_pChapterBtn[i]->runAction(Sequence::create(MoveBy::create(1.f, Vec2(-1 * winSize.width, 0)), NULL));
				if (getChildByTag(enSelectLevelSceneMenu_ChapterNum0 + i)){
					auto pLevelNumLabel = (Label*)getChildByTag(enSelectLevelSceneMenu_ChapterNum0 + i);
					pLevelNumLabel->runAction(Sequence::create(MoveBy::create(1.f, Vec2(-1 * winSize.width, 0)), NULL));
				}
				if (getChildByTag(enSelectLevelSceneMenu_ChapterLock0 + i)){
					auto pLevelLockSprite = (Sprite*)getChildByTag(enSelectLevelSceneMenu_ChapterLock0 + i);
					pLevelLockSprite->runAction(Sequence::create(MoveBy::create(1.f, Vec2(-1 * winSize.width, 0)), NULL));
				}
			}
			gLevelPageNum ++;
			if(gLevelPageNum > m_nLastPageNum) 
				gLevelPageNum = m_nLastPageNum;
			if(gLevelPageNum == m_nLastPageNum){
				m_pPrevItem->setEnabled(true);
				m_pNextItem->setEnabled(false);
			}
			else{

				m_pPrevItem->setEnabled(true);
				m_pNextItem->setEnabled(true);
			}
			page_index_enable->setPositionX(page_index[gLevelPageNum]->getPositionX());
			lbPageNum->setString(page_index_string[gLevelPageNum]);
			break;
		}
	case enSelectLevelSceneMenu_Unlock:
		{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
		//sdkbox::IAP::purchase("Unlock all game levels");
		 AdsManager::getInstance()->buyItem(UNLOCK_ALL_LEVELS);
#endif		
		}
	}

	if (nTag >= enSelectLevelSceneMenu_Chapter0){

		m_nTotalSelectChapter = nTag  - enSelectLevelSceneMenu_Chapter0;
		if(m_nTotalSelectChapter > m_nMaxChapter)
			m_nTotalSelectChapter = m_nMaxChapter;
		UserDefault::getInstance()->setIntegerForKey("total_select_chapter", m_nTotalSelectChapter);
		UserDefault::getInstance()->flush();

		Director *pDirector = Director::getInstance();
		Scene *pScene;
		if(first_install)
		{
			pScene = GuideScene::createScene();
			first_install=false;
			UserDefault::getInstance()->setBoolForKey("first_install",first_install);
			UserDefault::getInstance()->flush();
		}
		else
		{
			pScene = MainGameScene::createScene();
		}
		
		pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
	}
}
