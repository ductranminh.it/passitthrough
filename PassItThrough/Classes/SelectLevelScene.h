/************************************************************************
*  SelectLevelScene.h													
*																		
*																		
*  Created by Lin dao min											
*  Copyright Lin 2014-4-11											
*																		
************************************************************************/

#ifndef __SELECT_LEVEL_SCENE_H__
#define __SELECT_LEVEL_SCENE_H__

#include "common.h"

class SelectLevelScene : public cocos2d::Scene
{
public:
	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();  

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::Scene* createScene();

	void initLevelMenu();
	void onDrawChapters();
	void backCallBack(Ref * pSender);

	virtual void keyBackClicked();

	void onTimer(float dt);

	CREATE_FUNC(SelectLevelScene);

public:
	MenuItemImage *	m_pChapterBtn[LEVEL_KIND*CHAPTER_MAX];
	MenuItemImage *	m_pHomeItem;
	MenuItemImage *	m_pNextItem;
	MenuItemImage *	m_pPrevItem;
	MenuItemImage * m_pUnlockItem;
	Sprite* page_index_enable;
	Sprite* page_index[4];
	Label* lbPageNum;

	Menu * pMenu;

	void execute(EventCustom* event);

protected:
	int					m_nCompleteLevel;
	int					m_nCompleteChapter;
	int					m_nTotalCompleteChapter;

	int					m_nSelectedChapter;
	int                 m_nSelectedLevel;
	int					m_nMaxChapter;
	int					m_nTotalSelectChapter;
	int					m_nLastPageNum;

};


#endif  // __SELECT_LEVEL_SCENE_H__
