/************************************************************************
*  MainGameScene.cpp														
*																		
*																		
*  Created by udinsoft									
*  Copyright udinsoft 2019-15-5											
*																		
************************************************************************/
//#include <Winnls.h>
#include "MainGameScene.h"
#include "FirstScene.h"
#include "SelectLevelScene.h"
#include "SettingScene.h"
#include "adsManager.h"
#include "CloseScene.h"
#include "UnlockAllLevelsScene.h"
#include "PixelCollision.h"

enum enGameOverMenu{
	enGameOverMenu_Stage			=1,
	enGameOverMenu_Retry			=2,
	enGameOverMenu_Next				=3,
	enGameOverMenu_Facebook			=4,
	enGameOverMenu_Twitter          =5,
	enGameOverMenu_Google			=6,
    enOk                            =7,
    enClose                         =8,
    enMenu
	
};


using namespace cocos2d;
using namespace CocosDenshion;

MainGameScene* MainGameScene::create(){
	auto scene = new MainGameScene;
	scene->init();
	scene->autorelease();
	return scene;
}

MainGameScene::MainGameScene(){

	SimpleAudioEngine::getInstance()->preloadEffect("sound/alert.mp3");
}

MainGameScene::~MainGameScene(){
	SimpleAudioEngine::getInstance()->unloadEffect("sound/alert.mp3");
}

Scene* MainGameScene::createScene(){
	return MainGameScene::create();
}

// on "init" you need to initialize your instance
bool MainGameScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}
	if (remove_ad) {
		AdsManager::getInstance()->hideAdMob();
	}else
		AdsManager::getInstance()->showAdMob();


	ring_move_start = false;  ////when user moves the ring, this flag is true

	music_state = UserDefault::getInstance()->getBoolForKey("music_state", true);
	sound_state = UserDefault::getInstance()->getBoolForKey("sound_state", true);

	sound_volume = UserDefault::getInstance()->getFloatForKey("sound_volume", 5.0f);  ///10
	music_volume = UserDefault::getInstance()->getFloatForKey("music_volume", 4.0f);  ///8

	if (music_volume == 0.f){
		music_state = false;
		UserDefault::getInstance()->setBoolForKey("music_state", music_state);
	}
	else{
		music_state = true;
		UserDefault::getInstance()->setBoolForKey("music_state", music_state);
	}

	//log("sound_volume=%f", sound_volume);

	if (sound_volume<0.1f)
		sound_volume = 0.f;

	if (sound_volume == 0.f){
		sound_state = false;
		UserDefault::getInstance()->setBoolForKey("sound_state", sound_state);
	}
	else{
		sound_state = true;
		UserDefault::getInstance()->setBoolForKey("sound_state", sound_state);
	}

	if (music_state){
		if (!SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()){

			SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(music_volume / 8.f);
			SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/background.mp3", true);
		}
		else{

			SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(music_volume / 8.f);
			SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		}

	}
	if (sound_state)
		SimpleAudioEngine::getInstance()->setEffectsVolume(sound_volume / 10.f);
	////////chapter num display
	m_nTotalSelectChapter = UserDefault::getInstance()->getIntegerForKey("total_select_chapter", 0);
	m_nSelectedLevel = m_nTotalSelectChapter / CHAPTER_MAX;
	m_nSelectedChapter = m_nTotalSelectChapter % CHAPTER_MAX;

	char buf[64];
	sprintf(buf, "Round %d", m_nTotalSelectChapter + 1);

	auto lb_chapter = Label::createWithTTF(buf, fontList[3], 40, Size(winSize.width * 0.6f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_chapter->setVerticalAlignment(TextVAlignment::CENTER);
	CommonUtiles::setAdjustScale(lb_chapter);
	lb_chapter->setPosition(Vec2(winSize.width*0.12f, winSize.height*0.07f));
	lb_chapter->setColor(Color3B(254, 201, 33));
	addChild(lb_chapter, 1);

	///////// background

	Sprite* pBg;
	int nWidth = winSize.width;
	int nHeight = winSize.height;
	if (m_nSelectedLevel == 0)
	{
		if (nWidth % 1024 == 0 && nHeight % 768 == 0)
		{
			pBg = Sprite::create("common/bg1_1024x768.png");


		}
		else if (nWidth % 1280 == 0 && nHeight % 800 == 0)
		{
			pBg = Sprite::create("common/bg1_1280x800.png");

		}
		else if (nWidth % 800 == 0 && nHeight % 480 == 0)
		{
			pBg = Sprite::create("common/bg1_800x480.png");

		}
		else if (nWidth % 480 == 0 && nHeight % 320 == 0)
		{
			pBg = Sprite::create("common/bg1_960x640.png");

		}
		else if (nWidth % 568 == 0 && nHeight % 320 == 0)
		{
			pBg = Sprite::create("common/bg1_1136x640.png");

		}
		else
		{
			pBg = Sprite::create("common/bg1_1280x800.png");

		}
	}
	else if (m_nSelectedLevel == 1)
	{
		if (nWidth % 1024 == 0 && nHeight % 768 == 0)
		{
			pBg = Sprite::create("common/bg2_1024x768.png");


		}
		else if (nWidth % 1280 == 0 && nHeight % 800 == 0)
		{
			pBg = Sprite::create("common/bg2_1280x800.png");

		}
		else if (nWidth % 800 == 0 && nHeight % 480 == 0)
		{
			pBg = Sprite::create("common/bg2_800x480.png");

		}
		else if (nWidth % 480 == 0 && nHeight % 320 == 0)
		{
			pBg = Sprite::create("common/bg2_960x640.png");

		}
		else if (nWidth % 568 == 0 && nHeight % 320 == 0)
		{
			pBg = Sprite::create("common/bg2_1136x640.png");

		}
		else
		{
			pBg = Sprite::create("common/bg2_1280x800.png");

		}
	}
	else if (m_nSelectedLevel == 2)
	{
		if (nWidth % 1024 == 0 && nHeight % 768 == 0)
		{
			pBg = Sprite::create("common/bg3_1024x768.png");


		}
		else if (nWidth % 1280 == 0 && nHeight % 800 == 0)
		{
			pBg = Sprite::create("common/bg3_1280x800.png");

		}
		else if (nWidth % 800 == 0 && nHeight % 480 == 0)
		{
			pBg = Sprite::create("common/bg3_800x480.png");

		}
		else if (nWidth % 480 == 0 && nHeight % 320 == 0)
		{
			pBg = Sprite::create("common/bg3_960x640.png");

		}
		else if (nWidth % 568 == 0 && nHeight % 320 == 0)
		{
			pBg = Sprite::create("common/bg3_1136x640.png");

		}
		else
		{
			pBg = Sprite::create("common/bg3_1280x800.png");

		}
	}
	pBg->setScaleX(winSize.width / pBg->getContentSize().width * fScaleRetina);
	pBg->setScaleY(winSize.height / pBg->getContentSize().height * fScaleRetina);
	pBg->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));

	addChild(pBg, 0);

	pBg->setOpacity(180);

	//setting button

	m_pReturnHome = MenuItemImage::create("button/icon-home.png", "button/icon-home.png", CC_CALLBACK_1(MainGameScene::onReturnHome, this));
	CommonUtiles::setAdjustScale(m_pReturnHome);
	m_pReturnHome->setScale(m_pReturnHome->getScale() * 0.6f);
	m_pReturnHome->setPosition(Vec2(winSize.width * 0.06f, winSize.height * 0.9f));

	////pause button
	m_pPauseItem = MenuItemImage::create("button/pause.png", "button/pause.png", CC_CALLBACK_1(MainGameScene::onPause, this));
	CommonUtiles::setAdjustScale(m_pPauseItem);
	m_pPauseItem->setScale(m_pPauseItem->getScale() * 0.5f);
	m_pPauseItem->setPosition(Vec2(winSize.width * 0.9f, winSize.height * 0.9f));

	m_pPauseBoard = Sprite::create("button/pause-broad.png");
	CommonUtiles::setAdjustScale(m_pPauseBoard);
	m_pPauseBoard->setScale(m_pPauseItem->getScale() * 0.7f);
	m_pPauseBoard->setPosition(Vec2(winSize.width * 1.5f, winSize.height * 1.5f));
	addChild(m_pPauseBoard, 8);

	m_pPauseMask = Sprite::create("common/opacity.png");
	//CommonUtiles::setNormalScale(m_pPauseMask);
	m_pPauseMask->setScaleX(winSize.width / m_pPauseMask->getContentSize().width);
	m_pPauseMask->setScaleY(winSize.height / m_pPauseMask->getContentSize().height);
	m_pPauseMask->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	addChild(m_pPauseMask, 7);
	m_pPauseMask->setVisible(false);


	Size board_size = m_pPauseBoard->getContentSize();

	m_pResume = MenuItemImage::create("button/resume.png", "button/resume.png", CC_CALLBACK_1(MainGameScene::onResume, this));
	CommonUtiles::setAdjustScale(m_pResume);
	m_pResume->setScale(m_pResume->getScale() * 0.8f);
	m_pResume->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.7f));

	m_pRestart = MenuItemImage::create("button/restart.png", "button/restart.png", CC_CALLBACK_1(MainGameScene::onReplayItem, this));
	CommonUtiles::setAdjustScale(m_pRestart);
	m_pRestart->setScale(m_pRestart->getScale() * 0.8f);
	m_pRestart->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.55f));

	m_pOption = MenuItemImage::create("button/option.png", "button/option.png", CC_CALLBACK_1(MainGameScene::onPauseSetting, this));
	CommonUtiles::setAdjustScale(m_pOption);
	m_pOption->setScale(m_pOption->getScale() * 0.8f);
	m_pOption->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.4f));

	m_pStage = MenuItemImage::create("button/stage.png", "button/stage.png", CC_CALLBACK_1(MainGameScene::onPauseStage, this));
	CommonUtiles::setAdjustScale(m_pStage);
	m_pStage->setScale(m_pStage->getScale() * 0.8f);
	m_pStage->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.25f));

	pPauseMenu = Menu::create(m_pResume, m_pRestart, m_pOption, m_pStage, NULL);
	pPauseMenu->setPosition(Vec2::ZERO);
	m_pPauseBoard->addChild(pPauseMenu, 1);

	m_pReturnSelectlevel = MenuItemImage::create("button/icon-home.png", "button/icon-home.png", CC_CALLBACK_1(MainGameScene::onReturnSelectLevel, this));
	CommonUtiles::setAdjustScale(m_pReturnSelectlevel);
	m_pReturnSelectlevel->setScale(m_pReturnSelectlevel->getScale() * 0.6f);
	m_pReturnSelectlevel->setPosition(Vec2(winSize.width * 0.3f, winSize.height * 0.9f));


	/////right and left button

    auto button_between = Sprite::create("button/left_right_button.png");
	CommonUtiles::setAdjustScale(button_between);
	button_between->setScale(button_between->getScale()*0.3f);
	button_between->setPosition(Vec2(winSize.width * 0.9f, winSize.height * 0.1f));
	addChild(button_between, 2);

	Vec2 pos = button_between->getPosition();

	m_pLeftButton = MenuItemImage::create("button/button_left.png", "button/button_left.png", CC_CALLBACK_1(MainGameScene::onRotate, this));
	CommonUtiles::setAdjustScale(m_pLeftButton);
	m_pLeftButton->setScale(m_pLeftButton->getScale()*0.3f);
	m_pLeftButton->setAnchorPoint(Vec2(1.f, 0.5f));
	m_pLeftButton->setPosition(Vec2(pos.x - 10.f*fScaleN*fScaleRetina, pos.y));

	m_pRightButton = MenuItemImage::create("button/button_left.png", "button/button_left.png", CC_CALLBACK_1(MainGameScene::onRotate, this));
	CommonUtiles::setAdjustScale(m_pRightButton);
	m_pRightButton->setScale(m_pRightButton->getScale()*0.3f);
	m_pRightButton->setRotation(180);
	m_pRightButton->setAnchorPoint(Vec2(1.f, 0.5f));
	m_pRightButton->setPosition(Vec2(pos.x + 10.f*fScaleN*fScaleRetina, pos.y));

	pMenu = Menu::create(m_pPauseItem, m_pReturnHome, m_pLeftButton, m_pRightButton, NULL);
	pMenu->setPosition(Vec2::ZERO);
	addChild(pMenu, 1);

	/////////music and sound button

	m_pMusicOff = Sprite::create("button/icon-music-off.png");
	CommonUtiles::setAdjustScale(m_pMusicOff);
	m_pMusicOff->setScale(m_pMusicOff->getScale()*0.5f);
	m_pMusicOff->setPosition(Vec2(winSize.width * 0.75f, winSize.height * 0.9f));
	addChild(m_pMusicOff, 3);

	m_pMusicOn = Sprite::create("button/icon-music-on.png");
	CommonUtiles::setAdjustScale(m_pMusicOn);
	m_pMusicOn->setScale(m_pMusicOn->getScale()*0.5f);
	m_pMusicOn->setPosition(Vec2(winSize.width * 0.75f, winSize.height * 0.9f));
	addChild(m_pMusicOn, 3);
	if (!music_state)
		m_pMusicOn->setVisible(false);

	m_pSoundOff = Sprite::create("button/icon-sound-off.png");
	CommonUtiles::setAdjustScale(m_pSoundOff);
	m_pSoundOff->setScale(m_pSoundOff->getScale()*0.5f);
	m_pSoundOff->setPosition(Vec2(winSize.width * 0.82f, winSize.height * 0.9f));
	addChild(m_pSoundOff, 3);

	m_pSoundOn = Sprite::create("button/icon-sound-on.png");
	CommonUtiles::setAdjustScale(m_pSoundOn);
	m_pSoundOn->setScale(m_pSoundOn->getScale()*0.5f);
	m_pSoundOn->setPosition(Vec2(winSize.width * 0.82f, winSize.height * 0.9f));
	addChild(m_pSoundOn, 3);
	if (!sound_state)
		m_pSoundOn->setVisible(false);

	// ring image
	Sprite* temp = Sprite::create("common/ring2.png");
	Size tempSize = temp->getContentSize();

	m_pRingRight = Sprite::create("common/ring2.png", Rect(tempSize.width / 2, 0, tempSize.width / 2, tempSize.height));
	CommonUtiles::setAdjustScale(m_pRingRight);
	m_pRingRight->setAnchorPoint(Vec2(0.f, 0.5f));
	m_pRingRight->setScale(m_pRingRight->getScale() * 1.8f);
	m_pRingRight->setPosition(Vec2(30 * fScaleX*fScaleRetina, winSize.height * 0.58f));
	addChild(m_pRingRight, 4);

	m_pRingLeft = Sprite::create("common/ring2.png", Rect(0, 0, tempSize.width / 2, tempSize.height));
	CommonUtiles::setAdjustScale(m_pRingLeft);
	m_pRingLeft->setAnchorPoint(Vec2(1.f, 0.5f));
	m_pRingLeft->setScale(m_pRingLeft->getScale() * 1.8f);
	m_pRingLeft->setPosition(m_pRingRight->getPosition());
	addChild(m_pRingLeft, 1);

	////////////// top part of ring for collision checking 
	m_pRingUp = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingUp);
	m_pRingUp->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pRingUp->setPosition(Vec2(0, tempSize.height - (m_pRingUp->getBoundingBox().getMaxY() - m_pRingUp->getBoundingBox().getMinY()) / 2.f));		//0.62
	m_pRingRight->addChild(m_pRingUp, 5);
	m_pRingUp->setFlippedY(true);
	m_pRingUp->setOpacity(1);

	// bottom part of ring for collision checking 
	m_pRingDown = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingDown);
	m_pRingDown->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pRingDown->setPosition(Vec2(0, (m_pRingUp->getBoundingBox().getMaxY() - m_pRingUp->getBoundingBox().getMinY()) / 2.f));		//0.62
	m_pRingRight->addChild(m_pRingDown, 5);
	m_pRingDown->setOpacity(1);
	////////////////////////---temple
	m_pRingUpTemp = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingUpTemp);
	m_pRingUpTemp->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pRingUpTemp->setScale(m_pRingUpTemp->getScale() * 1.8f);
	m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
	addChild(m_pRingUpTemp, 5);
	m_pRingUpTemp->setFlippedY(true);
	//m_pRingUpTemp->setOpacity(1);

	// bottom part of ring for collision checking 
	m_pRingDownTemp = Sprite::create("common/ring_up2.png");
	CommonUtiles::setAdjustScale(m_pRingDownTemp);
	m_pRingDownTemp->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_pRingDownTemp->setScale(m_pRingDownTemp->getScale() * 1.8f);
	m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));//0.62
	addChild(m_pRingDownTemp, 5);
	m_pRingDownTemp->setOpacity(1);

	// tap image
	m_pTap = Sprite::create("common/handle.png");
	CommonUtiles::setAdjustScale(m_pTap);
	m_pTap->setScale(m_pTap->getScale() * 0.6f);
	m_pTap->setPosition(Vec2(30 * fScaleX*fScaleRetina, winSize.height * 0.37f));
	addChild(m_pTap, 4);

	// winner message
	m_pWinMessage = Sprite::create("common/win_panel.png");
	CommonUtiles::setAdjustScale(m_pWinMessage);
	m_pWinMessage->setScale(m_pWinMessage->getScale() * 0.4f);
	m_pWinMessage->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	addChild(m_pWinMessage, 5);
	m_pWinMessage->setVisible(false);

	// lose message
	m_pLoseMessage = Sprite::create("common/lose_panel.png");
	CommonUtiles::setAdjustScale(m_pLoseMessage);
	m_pLoseMessage->setScale(m_pLoseMessage->getScale() * 0.4f);
	m_pLoseMessage->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
	addChild(m_pLoseMessage, 5);
	m_pLoseMessage->setVisible(false);

	m_pSignBoard = Sprite::create("common/signbroad.png");
	CommonUtiles::setAdjustScale(m_pSignBoard);
	m_pSignBoard->setScale(m_pSignBoard->getScale() * 0.4f);
	m_pSignBoard->setAnchorPoint(Vec2(0.5f, 1.f));
	m_pSignBoard->setPosition(Vec2(winSize.width*0.5f, winSize.height));
	addChild(m_pSignBoard, 5);

	//score title and score num display
	auto lb_score_title = Label::createWithTTF("Score", fontList[3], 80, Size(winSize.width * 0.5f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_score_title->setVerticalAlignment(TextVAlignment::CENTER);
	lb_score_title->setPosition(Vec2(m_pSignBoard->getContentSize().width*0.5f, m_pSignBoard->getContentSize().height*0.63f));
	lb_score_title->setColor(Color3B(254, 201, 33));
	m_pSignBoard->addChild(lb_score_title, 1);


	m_nScore = UserDefault::getInstance()->getIntegerForKey("score", 0);
	sprintf(buf, "%d", m_nScore);
	lb_score = Label::createWithTTF(buf, fontList[3], 80, Size(winSize.width * 0.5f, winSize.height * 0.35f), TextHAlignment::CENTER);
	lb_score->setVerticalAlignment(TextVAlignment::CENTER);
	lb_score->setPosition(Vec2(m_pSignBoard->getContentSize().width*0.5f, m_pSignBoard->getContentSize().height*0.3f));
	lb_score->setColor(Color3B(254, 201, 33));
	m_pSignBoard->addChild(lb_score, 1);

	dis = 0.f;
	dis_temp = 0.f;
	m_nVibrateNum = 0;
	index = 0;
	stone_index = 0;
	m_pStone = NULL;
	touchStatus = false;

	initWire();

	auto touchListener = EventListenerTouchAllAtOnce::create();
	//touchListener->setSwallowTouches(true);
	touchListener->onTouchesBegan = CC_CALLBACK_2(MainGameScene::onTouchesBegan, this);
	touchListener->onTouchesEnded = CC_CALLBACK_2(MainGameScene::onTouchesEnded, this);
	touchListener->onTouchesMoved = CC_CALLBACK_2(MainGameScene::onTouchesMoved, this);
    touchListener->onTouchesCancelled = CC_CALLBACK_2(MainGameScene::onTouchesCancelled, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
    

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    sdkbox::PluginShare::setListener(this);
    sdkbox::PluginShare::init();
    sdkbox::PluginShare::setFileProviderAuthorities("com.magez.passitthrough.fileprovider");
#endif

    auto _listener1 = EventListenerCustom::create(EVENT_BUY_SUCCESS, std::bind(&MainGameScene::execute, this, std::placeholders::_1));
    if(_eventDispatcher->hasEventListener(EVENT_BUY_SUCCESS)){
        _eventDispatcher->removeCustomEventListeners(EVENT_BUY_SUCCESS);
    }
    _eventDispatcher->addEventListenerWithFixedPriority(_listener1, 1);

    auto _listener2 = EventListenerCustom::create(EVENT_BUY_CANCEL, std::bind(&MainGameScene::execute, this, std::placeholders::_1));
    if(_eventDispatcher->hasEventListener(EVENT_BUY_CANCEL)){
        _eventDispatcher->removeCustomEventListeners(EVENT_BUY_CANCEL);
    }
    _eventDispatcher->addEventListenerWithFixedPriority(_listener2, 1);

    auto _listener3 = EventListenerCustom::create(EVENT_BUY_FAILED, std::bind(&MainGameScene::execute, this, std::placeholders::_1));
    if(_eventDispatcher->hasEventListener(EVENT_BUY_FAILED)){
        _eventDispatcher->removeCustomEventListeners(EVENT_BUY_FAILED);
    }
    _eventDispatcher->addEventListenerWithFixedPriority(_listener3, 1);

	return true;
}

void MainGameScene::execute(EventCustom* event){
    std::string nameEvent = event->getEventName().c_str();
    if (nameEvent == EVENT_BUY_SUCCESS || nameEvent == EVENT_BUY_FAILED || nameEvent == EVENT_BUY_CANCEL) {
        showResult();
    }
}

void MainGameScene::keyBackClicked()
{
	//CCLog("back button clicked");
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
	pMenu->setEnabled(false);
	unscheduleAllCallbacks();
	stopAllActions();

	Director *pDirector = Director::getInstance();
	Scene *pScene = FirstScene::createScene();
	pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
}

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
void MainGameScene::afterCaptureScreen(bool yes, const std::string &outputFilename) {
    if (yes) {
        CCLOG("-------------SharePlugin capture screen success: %s", outputFilename.c_str());
        _captureFilename = outputFilename;
    }else {
        CCLOG("-------------SharePlugin capture screen unsuccess");
    }
}

void MainGameScene::onShareState(const sdkbox::SocialShareResponse& response)
{
    switch (response.state) {
        case sdkbox::SocialShareState::SocialShareStateNone: {
            CCLOG("SharePlugin::onShareState none");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateUnkonw: {
            CCLOG("SharePlugin::onShareState unkonw");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateBegin: {
            CCLOG("SharePlugin::onShareState begin");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateSuccess: {
            CCLOG("SharePlugin::onShareState success");
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateFail: {
            CCLOG("SharePlugin::onShareState fail, error:%s", response.error.c_str());
            break;
        }
        case sdkbox::SocialShareState::SocialShareStateCancelled: {
            CCLOG("SharePlugin::onShareState cancelled");
            break;
        }
        case sdkbox::SocialShareStateSelectShow: {
            CCLOG("SharePlugin::onShareState show pancel %d", response.platform);
            break;
        }
        case sdkbox::SocialShareStateSelectCancelled: {
            CCLOG("SharePlugin::onShareState show pancel cancelled %d", response.platform);
            break;
        }
        case sdkbox::SocialShareStateSelected: {
            CCLOG("SharePlugin::onShareState show pancel selected %d", response.platform);
            break;
        }
        default: {
            CCLOG("SharePlugin::onShareState");
            break;
        }
    }
}

#endif

int MainGameScene::RandomBetweenAandB(int a,int b)
{
	if(a>b)
	{
		int c=b;
		b=a;
		a=c;
	}
	return a+rand()%(b-a+1);
}

void MainGameScene::initWire()
{
	num=curve_num[m_nSelectedLevel][m_nSelectedChapter];
	//CCLog("curve num=%d",curve_num[m_nSelectedLevel][m_nSelectedChapter]);
	float zoomx=0.7f;
	// wire spline
	int item;
	for(int order=0;order<=num-1;order++)
	{
		if(order==0)
			item=1;
		else if(order==num-1)
			item=37;
		else
			item=RandomBetweenAandB(2,curve_difficulty[m_nSelectedChapter]);

		char buf[256];
		//CCLog("wire_num=%d",item);
		sprintf(buf,"curve/curve%d.png",item);
		m_pWire[order] = Sprite::create(buf);

		CommonUtiles::setAdjustScale(m_pWire[order]);
		m_pWire[order]->setScale(m_pWire[order]->getScale() *zoomx);		
		m_pWire[order]->setAnchorPoint(Vec2(0.f,0.5f));
		pattern_width=m_pWire[order]->getBoundingBox().getMaxX()-m_pWire[order]->getBoundingBox().getMinX();
		pattern_height=m_pWire[order]->getContentSize().height;
		m_pWire[order]->setPosition(Vec2(pattern_width*order, winSize.height * 0.6f));
		addChild(m_pWire[order], 2);

	}
	startPointX=0.f;                 /////wire start point x
	endPointX=pattern_width*num; /////wire end point x 
	displayEndPointX=winSize.width;

	schedule(schedule_selector(MainGameScene::DragonAppear),0.1f);

	scheduleUpdate();

}

void MainGameScene::DragonAppear(float dt){

	index++;

	if(index%10==0){

		index=0;
		int posx=RandomBetweenAandB(80*fScaleN,winSize.width-80*fScaleN);
		Sprite* m_pDragon=Sprite::create("common/dragon.png");
		CommonUtiles::setAdjustScale(m_pDragon);
		m_pDragon->setScale(m_pDragon->getScale()*0.3f);
		m_pDragon->setPosition(Vec2(posx,10.f));
		addChild(m_pDragon,5);
		dragon_array.pushBack(m_pDragon);
	}

	for(int i=0;i<dragon_array.size();i++){

		auto sprite=(Sprite*)dragon_array.at(i);
		Vec2 pos=sprite->getPosition();
		sprite->setPosition(Vec2(pos.x+RandomBetweenAandB(0,10)-5.f,pos.y+5.f));
		if(sprite->getPositionY()>winSize.height){
			dragon_array.erase(i);
			removeChild(sprite);
		}
	}
}

void MainGameScene::StoneAppear(float dt){

	char buf[32];
	stone_index++;

	if(stone_index%120==0){
		stone_index=0;
		int posx=RandomBetweenAandB(m_pRingLeft->getPositionX()-50*fScaleN,m_pRingLeft->getPositionX()+50*fScaleN);
		int m_stoneKind=RandomBetweenAandB(1,3);
		sprintf(buf,"rock%d.png",m_stoneKind);
		if(m_pStone)
			removeChild(m_pStone);
		m_pStone=Sprite::create(buf);
		CommonUtiles::setAdjustScale(m_pStone);
		m_pStone->setScale(m_pStone->getScale()*0.5f);
		m_pStone->setPosition(Vec2(posx,winSize.height));
		addChild(m_pStone,3);
		
	}
	else{
		if(m_pStone)
			m_pStone->setPositionY(m_pStone->getPositionY()-5.f*fScaleY);
	}

}


void MainGameScene::VibrateDevice(float dt){

	int m_nTime=RandomBetweenAandB(1000,3000);
	long long lTime = (long long)m_nTime;
}

void MainGameScene::DelayMoveAnimation(){
	
	ring_move_start=true;
	move_len=endPointX-displayEndPointX;

	if( ! bIsCollision){

		schedule(schedule_selector(MainGameScene::WireMoveAnimation),WIRE_MOVE_TIME_INTERVAL);
		
		if(m_nTotalSelectChapter>49 && m_nTotalSelectChapter<60){

			schedule(schedule_selector(MainGameScene::StoneAppear),0.1f);
		}
	}
}

void MainGameScene::WireMoveAnimation(float dt){

	move_len-=WIRE_MOVE_STEP;
	if(m_nTotalSelectChapter>19){

		if(m_nTotalSelectChapter<23)
			vibrate_height=1;
		else if(m_nTotalSelectChapter>=23 && m_nTotalSelectChapter<27)
			vibrate_height=2;
		else if(m_nTotalSelectChapter>=27 && m_nTotalSelectChapter<30)
			vibrate_height=3;
		else if(m_nTotalSelectChapter>=30 && m_nTotalSelectChapter<37)
			vibrate_height=3;
		else if(m_nTotalSelectChapter>=37 && m_nTotalSelectChapter<43)
			vibrate_height=4;
		else if(m_nTotalSelectChapter>=43 && m_nTotalSelectChapter<50)
			vibrate_height=5;
	}
	else{
		vibrate_height=1;
	}
	int step=vibrate();

	if(m_nTotalSelectChapter>19 && m_nTotalSelectChapter<30){

		m_pTap->setPositionY(m_pTap->getPositionY()+step*vibrate_height);
		m_pRingLeft->setPosition(Vec2(m_pRingLeft->getPositionX(),m_pRingLeft->getPositionY()+step*vibrate_height));
		m_pRingRight->setPosition(m_pRingLeft->getPosition());
		m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
		m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));
	}

	for(int i=0;i<num;i++){

		if(move_len>0.f){

			m_pWire[i]->setPositionX(m_pWire[i]->getPositionX()-WIRE_MOVE_STEP);
		}

		if(m_nTotalSelectChapter>=30 && m_nTotalSelectChapter<50 && ring_move_start){

				m_pWire[i]->setPositionY(m_pWire[i]->getPositionY()+step*vibrate_height);
		}	
	}
}

int MainGameScene::vibrate(){
	
	m_nVibrateNum++;
	if((m_nVibrateNum+vibrate_height)%(vibrate_height*4)<vibrate_height*2+1)
		return 1;
	else
		return -1;
}

void MainGameScene::onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event){
    Vec2 touchLocation = touches[0]->getLocationInView();
    touchLocation = Director::getInstance()->convertToGL(touchLocation);
	//playBtnClkSound(2);
    Vec2 pos = touchLocation;//convertTouchToNodeSpace(touch);
	if(touchStatus)
		return;
	if (m_pTap->getBoundingBox().containsPoint(pos))
	{
		touchStart = pos;
		touchStatus = true;
		moveOldPoint=touchStart;
		initTapPos = m_pTap->getPosition();
		initRingPos = m_pRingLeft->getPosition();
		return;
	}
	else if(m_pSoundOn->getBoundingBox().containsPoint(pos))
	{
		touchStatus = true;
		sound_state_changed=true;
		if(sound_state)
		{
			sound_state=false;
			m_pSoundOn->setVisible(false);
			sound_volume=0.f;
		}
		else
		{
			sound_state=true;
			m_pSoundOn->setVisible(true);
			sound_volume=10.f;
		}
        SimpleAudioEngine::getInstance()->setEffectsVolume(sound_volume);
		UserDefault::getInstance()->setBoolForKey("sound_state",sound_state);
		UserDefault::getInstance()->setFloatForKey("sound_volume",sound_volume);
		UserDefault::getInstance()->flush();
		return;
	}
	else if (m_pMusicOn->getBoundingBox().containsPoint(pos))
	{
		touchStatus = true;
		music_state_changed=true;
		if(music_state)
		{
			music_state=false;
			m_pMusicOn->setVisible(false);
			music_volume=0.f;
			SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		}
		else
		{
			music_state=true;
			m_pMusicOn->setVisible(true);
			music_volume=8.f;

            if (!SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()){
                SimpleAudioEngine::getInstance()->playBackgroundMusic("sound/background.mp3", true);
            }
            else{

                SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
            }
		}
        SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(music_volume);
		UserDefault::getInstance()->setBoolForKey("music_state",music_state);
		UserDefault::getInstance()->setFloatForKey("music_volume",music_volume);
		UserDefault::getInstance()->flush();
		return;
	}

	//return false;
	
}

void MainGameScene::onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event)
{
    Vec2 touchLocation = touches[0]->getLocationInView();
    touchLocation = Director::getInstance()->convertToGL(touchLocation);
    Vec2 curPos = touchLocation;//convertTouchToNodeSpace(touch);
	Vec2 psub;
	Vec2 psubTap;
	Vec2 psubRing;
	if(touchStatus)
	{
		if (m_pTap->getBoundingBox().containsPoint(curPos))
		{
			if(!ring_move_start)
				DelayMoveAnimation();

			fDistance = curPos.getDistance(moveOldPoint);
			if(fDistance < 20.f * fScaleN)
			{
				psub = touchStart - initTapPos;		// tap init delta
				psubTap = curPos - psub;				// current tap position
				m_pTap->setPosition(psubTap);

				psub = touchStart - initRingPos;		
				psubRing = curPos - psub;
				m_pRingLeft->setPosition(psubRing);
				m_pRingRight->setPosition(psubRing);
				m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
				m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));
				moveOldPoint = curPos;
			}
		}	
	}
	else
		touchStatus=false;
	
}

void MainGameScene::onTouchesCancelled(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event)
{
    onTouchesEnded(touches, event);
}

void MainGameScene::onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event  *event){
    Vec2 touchLocation = touches[0]->getLocationInView();
    touchLocation = Director::getInstance()->convertToGL(touchLocation);
    Vec2 curPos = touchLocation;//convertTouchToNodeSpace(touchLocation);
	touchStatus=false;
}

void MainGameScene::didAccelerate(Acceleration* pAccelerationValue) {

//	//CCLog("%f,%f,%f",pAccelerationValue->x,pAccelerationValue->y,pAccelerationValue->z);


/*	dis=pAccelerationValue->x*90.f;
	//CCLog("accelerateX=%f",dis);
//	CCAction* rotate_action1=RotateBy::create(0.1f,dis/5.f);
//	rotate_action1->retain();
//	CCAction* rotate_action2=RotateBy::create(0.1f,dis/5.f);
//	rotate_action2->retain();
//	m_pRingLeft->runAction(rotate_action1);
//	m_pRingRight->runAction(rotate_action2);
	if(abs(dis-dis_temp)>2.f)
	{
		float rotate_angle;
		if(dis>dis_temp)
			rotate_angle=5.f;
		else
			rotate_angle=-5.f;
		m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
		m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));
		m_pRingUpTemp->setRotation(m_pRingLeft->getRotation()+rotate_angle);
		m_pRingDownTemp->setRotation(m_pRingLeft->getRotation()+rotate_angle);
		m_pRingLeft->setRotation(m_pRingLeft->getRotation() + rotate_angle);
		m_pRingRight->setRotation(m_pRingRight->getRotation() + rotate_angle);
		if(m_pRingLeft->getRotation() > 90.f)
			m_pRingLeft->setRotation(90.f);
		if(m_pRingLeft->getRotation() < -90.f)
			m_pRingLeft->setRotation(- 990.f);
		if(m_pRingRight->getRotation() >0.f)
			m_pRingRight->setRotation(90.f);
		if(m_pRingRight->getRotation() < -90.f)
			m_pRingRight->setRotation(-90.f);
	}
	
	
	dis_temp=dis;
//	dis=0;
*/
	
}

void MainGameScene::onRotate(Ref* pSender){
	float rotate_angle;
	auto item = dynamic_cast<MenuItemImage*>(pSender);
	
	if(item == m_pRightButton)
	{
		rotate_angle=10.f;
	}
	else
	{
		rotate_angle=-10.f;
	}

	m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
	m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));
	m_pRingUpTemp->setRotation(m_pRingLeft->getRotation()+rotate_angle);
	m_pRingDownTemp->setRotation(m_pRingLeft->getRotation()+rotate_angle);
	m_pRingLeft->setRotation(m_pRingLeft->getRotation() + rotate_angle);
	m_pRingRight->setRotation(m_pRingRight->getRotation() + rotate_angle);
	if(m_pRingLeft->getRotation() > 45.f)
		m_pRingLeft->setRotation(45.f);
	if(m_pRingLeft->getRotation() < -45.f)
		m_pRingLeft->setRotation(- 45.f);
	if(m_pRingRight->getRotation() >45.f)
		m_pRingRight->setRotation(45.f);
	if(m_pRingRight->getRotation() < -45.f)
		m_pRingRight->setRotation(-45.f);
}

void MainGameScene::onReplayItem(Ref* pSender){
	Director *pDirector = Director::getInstance();
	Scene *pScene = MainGameScene::createScene();
	pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
}

void MainGameScene::onPause(Ref* pSender)
{
	//pauseSchedulerAndActions();
	this->pause();
	m_pPauseMask->setVisible(true);
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
	pMenu->setEnabled(false);
	m_pPauseBoard->setPosition(Vec2(winSize.width*0.5f,winSize.height*0.5f));
	m_pPauseBoard->setVisible(true);
}

void MainGameScene::onResume(Ref* pSender)
{
	//resumeSchedulerAndActions();
	this->resume();
	m_pPauseMask->setVisible(false);
	Director::getInstance()->getEventDispatcher()->resumeEventListenersForTarget(this);
	pMenu->setEnabled(true);
	m_pPauseBoard->setPosition(Vec2(winSize.width*1.5f,winSize.height*1.5f));
	m_pPauseBoard->setVisible(true);
}

void MainGameScene::onPauseSetting(Ref* pSender)
{
	//resumeSchedulerAndActions();
	this->resume();
	m_pPauseMask->setVisible(false);
	m_pPauseBoard->setPosition(Vec2(winSize.width*1.5f,winSize.height*1.5f));
	m_pPauseBoard->setVisible(true);
	onSetting(pSender);
}

void MainGameScene::onPauseStage(Ref* pSender)
{
	level_or_chapter=false;
	Director *pDirector = Director::getInstance();
	Scene *pScene = SelectLevelScene::createScene();
	pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
}

void MainGameScene::onSetting(Ref* pSender)
{
	menu_halt=true;
	Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
	pMenu->setEnabled(false);
	Director::getInstance()->pause();
	auto layer=SettingScene::createScene();
	layer->setPosition(Vec2::ZERO);
	addChild(layer,7);
	schedule(schedule_selector(MainGameScene::onTimer),0.1f);

}
void MainGameScene::onReturnSelectLevel(Ref* pSender)
{
	level_or_chapter=false;
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();


	Director *pDirector = Director::getInstance();
	Scene *pScene = SelectLevelScene::createScene();
	pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
}

void MainGameScene::onReturnHome(Ref* pSender)
{
	level_or_chapter=false;
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	Director *pDirector = Director::getInstance();
	Scene *pScene = FirstScene::createScene();
	pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
}

void MainGameScene::onTimer(float dt)
{
	if(!menu_halt)
	{
		pMenu->setEnabled(true);
		Director::getInstance()->getEventDispatcher()->resumeEventListenersForTarget(this);
		unschedule(schedule_selector(MainGameScene::onTimer));

		if(music_volume==0.f)
		{
			music_state=false;
			UserDefault::getInstance()->setBoolForKey("music_state", music_state);
			m_pMusicOn->setVisible(false);

		}
		else 
		{
			music_state=true;
			UserDefault::getInstance()->setBoolForKey("music_state", music_state);
			m_pMusicOn->setVisible(true);
		}

		//CCLog("sound_volume=%f",sound_volume);
		if(sound_volume<0.1f)
			sound_volume=0.f;

		if(sound_volume==0.f)
		{
			sound_state=false;
			UserDefault::getInstance()->setBoolForKey("sound_state", sound_state);
			m_pSoundOn->setVisible(false);
		}
		else 
		{
			sound_state=true;
			UserDefault::getInstance()->setBoolForKey("sound_state", sound_state);
			m_pSoundOn->setVisible(true);
		}
	}
}


void MainGameScene::update(float dt) 
{
	
	bIsCollision = false;
	m_pRingUpTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingUp->getPosition()));
	m_pRingDownTemp->setPosition(m_pRingRight->convertToWorldSpace(m_pRingDown->getPosition()));

	for (int i = 0; i < num; i++) {
		if (PixelCollision::getInstance()->collidesWithSprite(m_pRingUpTemp, m_pWire[i], true)) {
			bIsCollision = true;
			break;
		}
		if(PixelCollision::getInstance()->collidesWithSprite(m_pRingDownTemp, m_pWire[i], true)) {
			bIsCollision = true;
			break;
		}
	}

	if (m_pStone)
	{
		if (PixelCollision::getInstance()->collidesWithSprite(m_pRingUpTemp, m_pStone, true)) {
			bIsCollision = true;

		}
		if (PixelCollision::getInstance()->collidesWithSprite(m_pRingDownTemp, m_pStone, true)) {
			bIsCollision = true;
		}
	}


	if (bIsCollision){
		if(sound_state)
			SimpleAudioEngine::getInstance()->playEffect("sound/alert.mp3");
		if(music_state)
			SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

		unscheduleAllCallbacks();
		stopAllActions();
		Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);

		showGameOver(false);


		m_pRightButton->setEnabled(false);
		m_pLeftButton->setEnabled(false);

	}
	if ((m_pRingLeft->getPositionX() > winSize.width-30*fScaleX*fScaleRetina )||(m_pRingLeft->getPositionX() >endPointX-30*fScaleX)){
		unscheduleAllCallbacks();
		stopAllActions();

		Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);

		m_nTotalCompleteChapter=UserDefault::getInstance()->getIntegerForKey("total_complete_chapter", 0);
		m_nTotalSelectChapter++;
		if(m_nTotalSelectChapter>=m_nTotalCompleteChapter)
		{
			m_nTotalCompleteChapter++;
			if(m_nTotalCompleteChapter>59) 
				m_nTotalCompleteChapter=59;
			
		}		

		UserDefault::getInstance()->setIntegerForKey("total_complete_chapter",m_nTotalCompleteChapter);
		UserDefault::getInstance()->setIntegerForKey("total_select_chapter",m_nTotalSelectChapter);
		
		UserDefault::getInstance()->setIntegerForKey("score",m_nScore);
		UserDefault::getInstance()->flush();
		int high_score=UserDefault::getInstance()->getIntegerForKey("high_score",0);
		if(m_nScore>high_score)
			UserDefault::getInstance()->setIntegerForKey("high_score",m_nScore);
		UserDefault::getInstance()->flush();
		
		//CCLog("");
		m_pRightButton->setEnabled(false);
		m_pLeftButton->setEnabled(false);

        showGameOver(true);

	}
	if (m_pRingLeft->getPositionX() < 30*fScaleX*fScaleRetina){
		m_pRingLeft->setPositionX(30*fScaleX*fScaleRetina);
		m_pRingRight->setPositionX(30*fScaleX*fScaleRetina);
		m_pTap->setPositionX(30*fScaleX*fScaleRetina);
	}

	if (m_pRingRight->getPositionX() > winSize.width-30.f*fScaleN){
		m_pRingLeft->setPositionX(winSize.width-30.f*fScaleN);
		m_pRingRight->setPositionX(winSize.width-30.f*fScaleN);

		m_pTap->setPositionX(winSize.width-30.f*fScaleN);
	}

	for(int i=0;i<dragon_array.size();i++)
	{
		auto dragon=(Sprite*)dragon_array.at(i);
		if (dragon) {
			if (PixelCollision::getInstance()->collidesWithSprite(m_pRingRight, dragon, true) || PixelCollision::getInstance()->collidesWithSprite(m_pRingLeft, dragon, true)) {
				dragon_array.at(i);
				drawBonus(dragon->getPosition());
				dragon_array.eraseObject(dragon);
				removeChild(dragon);
			}
		}
	}
}

void MainGameScene::showUnlockAllLevels(){

    unlock_background = Sprite::create("pay_screen/unlock-level-broad.png");
    unlock_background->setPosition(Vec2(winSize.width *0.5f, winSize.height * 0.5f));
    CommonUtiles::setAdjustScale(unlock_background);
    unlock_background->setScale(unlock_background->getScale()*0.6f);
    addChild(unlock_background, 8);
    float m_nWidth = unlock_background->getContentSize().width;
    float m_nHeight = unlock_background->getContentSize().height;

    //ok menu
    MenuItemImage* btn_ok = MenuItemImage::create("option_screen/ok.png", "option_screen/ok.png", CC_CALLBACK_1(MainGameScene::backCallBack, this));
    btn_ok->setTag(enOk);
    btn_ok->setPosition(Vec2(m_nWidth*0.75f, m_nHeight*0.05f));

    //cancel
    MenuItemImage* btn_cancel = MenuItemImage::create("option_screen/close.png", "option_screen/close.png", CC_CALLBACK_1(MainGameScene::backCallBack, this));
    btn_cancel->setTag(enClose);
    btn_cancel->setPosition(Vec2(m_nWidth*0.25f, m_nHeight*0.05f));

    auto m_pMainMenu = Menu::create(btn_ok, btn_cancel, NULL);
    m_pMainMenu->setPosition(Vec2::ZERO);
    m_pMainMenu->setTag(enMenu);
    unlock_background->addChild(m_pMainMenu, 3);

}

void MainGameScene::drawBonus(Vec2 pos)
{
	auto m_pBonus=Sprite::create("common/bonus.png");
	m_pBonus->setPosition(pos);
	m_pBonus->runAction(MoveBy::create(1.f,Vec2(0.f,50.f)));
	m_pBonus->runAction(Sequence::create(FadeIn::create(1.0f), CallFunc::create(CC_CALLBACK_0(MainGameScene::removeBonus,this,m_pBonus)), NULL));
	addChild(m_pBonus,6);
}

void MainGameScene::removeBonus(Node* sender)
{
	removeChild(sender);
	m_nScore+=10;
	char buf[64];
	sprintf(buf,"%d",m_nScore);
	lb_score->setString(buf);
}

void MainGameScene::nextChapter(Node* sender)
{
	Director *pDirector = Director::getInstance();
	Scene *pScene = MainGameScene::createScene();
	pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
	AdsManager::getInstance()->showAdMob();
}


void MainGameScene::setValue()
{
	int k=5;
	for(int i=0;i<5;i++)
	{
		if(m_nScore>high_score[i])
		{
			k=i;
			break;
		}
	}
	for(int i=4;i>k;i--)
	{
		high_score[i]=high_score[i-1];
		high_time[i]=high_time[i-1];
	}
	if(k!=5)
	{
		high_score[k]=m_nScore;
		
		time_t rawtime;
		struct tm * timeinfo;
		time (&rawtime);
		timeinfo = localtime (&rawtime);
		char buf[50];
		sprintf(buf,"%2d/%2d/%4d %2d:%2d",timeinfo->tm_mday,timeinfo->tm_mon+1,timeinfo->tm_year+1900,timeinfo->tm_hour,timeinfo->tm_min);
		high_time[k]=buf;
		
		
	}
	UserDefault::getInstance()->setIntegerForKey("highscore", high_score[0]);
	UserDefault::getInstance()->setIntegerForKey("secondscore",high_score[1]);
	UserDefault::getInstance()->setIntegerForKey("thirdscore",high_score[2]);
	UserDefault::getInstance()->setIntegerForKey("forthscore",high_score[3]);
	UserDefault::getInstance()->setIntegerForKey("fifthscore",high_score[4]);

	UserDefault::getInstance()->setStringForKey("hightime", high_time[0]);
	UserDefault::getInstance()->setStringForKey("secondtime",high_time[1]);
	UserDefault::getInstance()->setStringForKey("thirdtime",high_time[2]);
	UserDefault::getInstance()->setStringForKey("forthtime",high_time[3]);
	UserDefault::getInstance()->setStringForKey("fifthtime",high_time[4]);



	UserDefault::getInstance()->flush();
	char buf[32];
	/*for(int i=0;i<5;i++)
	{
		//CCLog("score_order[%d]=%d",i,high_score[i]);
	}*/
}

void MainGameScene::showResult(){
#if (COCOS2D_VERSION > 0x00030000)
    std::string path = "screenshot.png";
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    path = FileUtils::getInstance()->getWritablePath() + "screenshot.png";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    utils::captureScreen(CC_CALLBACK_2(MainGameScene::afterCaptureScreen, this), path);
#endif
#endif

    if(result)
        m_pGameOverBoard = Sprite::create("game_over/broad_youpassed.png");
    else
    {
        m_pGameOverBoard = Sprite::create("game_over/broad_gameover.png");
        setValue();
    }
    win_or_lose=result;
    CommonUtiles::setAdjustScale(m_pGameOverBoard);
    m_pGameOverBoard->setScale(m_pGameOverBoard->getScale() * 0.5f);

    m_pGameOverBoard->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.5f));
    addChild(m_pGameOverBoard,8);

    Size board_size=m_pGameOverBoard->getContentSize();

    /////////////share on
    auto m_pShareOn = Sprite::create("button/share-on.png");
    m_pShareOn->setPosition(Vec2(board_size.width * 0.25f, board_size.height * 0.25f));
    m_pShareOn->setScale(m_pShareOn->getScale()*0.5f);
    m_pGameOverBoard->addChild(m_pShareOn, 2);


    ///score label
    auto lb_score= Label::createWithTTF("Your score",fontList[3],60, Size(winSize.width * 0.7f, winSize.height * 0.35f), TextHAlignment::CENTER);
    lb_score->setVerticalAlignment(TextVAlignment::CENTER);
    lb_score->setPosition( Vec2(board_size.width*0.35f, board_size.height*0.6f));
    lb_score->setColor(Color3B(255,255,255));
    m_pGameOverBoard->addChild(lb_score,1);

    auto lb_highscore= Label::createWithTTF("high score",fontList[3],60, Size(winSize.width * 0.7f, winSize.height * 0.35f), TextHAlignment::CENTER);
    lb_highscore->setVerticalAlignment(TextVAlignment::CENTER);
    lb_highscore->setPosition( Vec2(board_size.width*0.35f, board_size.height*0.4f));
    lb_highscore->setColor(Color3B(255,255,255));
    m_pGameOverBoard->addChild(lb_highscore,1);

    char buf[30];
    sprintf(buf,"%d",m_nScore);
    auto your_score= Label::createWithTTF(buf,fontList[3],60, Size(winSize.width * 0.3f, winSize.height * 0.35f), TextHAlignment::CENTER);
    your_score->setVerticalAlignment(TextVAlignment::CENTER);
    your_score->setPosition( Vec2(board_size.width*0.7f, board_size.height*0.6f));
    your_score->setColor(Color3B(0,62,98));
    m_pGameOverBoard->addChild(your_score,1);

    sprintf(buf,"%d",UserDefault::getInstance()->getIntegerForKey("high_score",0));
    auto high_score= Label::createWithTTF(buf,fontList[3],60, Size(winSize.width * 0.3f, winSize.height * 0.35f), TextHAlignment::CENTER);
    high_score->setVerticalAlignment(TextVAlignment::CENTER);
    high_score->setPosition( Vec2(board_size.width*0.7f, board_size.height*0.4f));
    high_score->setColor(Color3B(0,62,98));
    m_pGameOverBoard->addChild(high_score,1);

    //////button
    Sprite* pSprite1 = Sprite::create("game_over/stage_button.png");
    Sprite* pSprite2 = Sprite::create("game_over/stage_button.png");
    pSprite2->setOpacity(150);
    MenuItemSprite* m_pGMStageItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(MainGameScene::backCallBack, this));
    m_pGMStageItem->setScale(m_pGMStageItem->getScale() * 0.8f);
    m_pGMStageItem->setPosition(Vec2(board_size.width * 0.2f, board_size.height * 0.05f));
    m_pGMStageItem->setTag(enGameOverMenu_Stage);

    pSprite1 = Sprite::create("game_over/retry_button.png");
    pSprite2 = Sprite::create("game_over/retry_button.png");
    pSprite2->setOpacity(150);
    MenuItemSprite* m_pGMRetryItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(MainGameScene::backCallBack, this));
    m_pGMRetryItem->setScale(m_pGMRetryItem->getScale() * 0.8f);
    m_pGMRetryItem->setPosition(Vec2(board_size.width * 0.8f, board_size.height * 0.05f));
    m_pGMRetryItem->setTag(enGameOverMenu_Retry);


    pSprite1 = Sprite::create("game_over/next_button.png");
    pSprite2 = Sprite::create("game_over/next_button.png");
    pSprite2->setOpacity(150);
    MenuItemSprite* m_pGMNextItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(MainGameScene::backCallBack, this));
    m_pGMNextItem->setScale(m_pGMNextItem->getScale() * 0.8f);
    m_pGMNextItem->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.05f));
    m_pGMNextItem->setTag(enGameOverMenu_Next);

    // facebook button
    pSprite1 = Sprite::create("button/facebook.png");
    pSprite2 = Sprite::create("button/facebook.png");
    pSprite2->setOpacity(150);
    MenuItemSprite* m_pGMFacebookItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(MainGameScene::backCallBack, this));
    m_pGMFacebookItem->setScale(m_pGMFacebookItem->getScale() * 0.7f);
    m_pGMFacebookItem->setPosition(Vec2(board_size.width * 0.5f, board_size.height * 0.25f));
    m_pGMFacebookItem->setTag(enGameOverMenu_Facebook);

    // twitter button
    pSprite1 = Sprite::create("button/twitter.png");
    pSprite2 = Sprite::create("button/twitter.png");
    pSprite2->setOpacity(150);
    MenuItemSprite* m_pGMTwitterItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(MainGameScene::backCallBack, this));
    m_pGMTwitterItem->setScale(m_pGMTwitterItem->getScale() * 0.7f);
    m_pGMTwitterItem->setPosition(Vec2(board_size.width * 0.65f, board_size.height * 0.25f));
    m_pGMTwitterItem->setTag(enGameOverMenu_Twitter);

    // google button
    pSprite1 = Sprite::create("button/google.png");
    pSprite2 = Sprite::create("button/google.png");
    pSprite2->setOpacity(150);
    MenuItemSprite* m_pGMGoogleItem = MenuItemSprite::create(pSprite1, pSprite2, CC_CALLBACK_1(MainGameScene::backCallBack, this));
    m_pGMGoogleItem->setScale(m_pGMGoogleItem->getScale() * 0.7f);
    m_pGMGoogleItem->setPosition(Vec2(board_size.width * 0.8f, board_size.height * 0.25f));
    m_pGMGoogleItem->setTag(enGameOverMenu_Google);

    if(result)
        pGameOverMenu = Menu::create(m_pGMStageItem,m_pGMRetryItem,m_pGMNextItem,m_pGMFacebookItem,m_pGMTwitterItem, NULL );
    else
        pGameOverMenu = Menu::create(m_pGMStageItem,m_pGMRetryItem,m_pGMFacebookItem,m_pGMTwitterItem, NULL );
    pGameOverMenu->setPosition(Vec2::ZERO);
    m_pGameOverBoard->addChild(pGameOverMenu, 1);
}

void MainGameScene::showGameOver(bool bWin)
{
	AdsManager::getInstance()->hideAdMob();
    result = bWin;
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
    if(is_iap_enabled)
    {
        if(m_nTotalSelectChapter == 1){
            if(unlock_all){
                showResult();
            } else
            {
                bool request = UserDefault::getInstance()->getBoolForKey("request_unlock", false);
                if (!request )
                {
                    if(result)
                    {
                        showUnlockAllLevels();
                        UserDefault::getInstance()->setBoolForKey("request_unlock", true);
                    }else
                        showResult();
                }
                else {
                    showResult();
                }
            }
        } else
            showResult();
    } else{
        showResult();
    }
#else
    showResult();
#endif

}

void MainGameScene::backCallBack(Ref* pSender)
{
	auto buf=(MenuItemSprite*)pSender;
	switch(buf->getTag())
	{
	case enGameOverMenu_Stage:
		{
			SimpleAudioEngine::getInstance()->stopBackgroundMusic();
			Director *pDirector = Director::getInstance();
			Scene *pScene = SelectLevelScene::createScene();
			pDirector->replaceScene(TransitionFade::create(0.5f, pScene));
			break;
		}
	case enGameOverMenu_Retry:
		{
			if(win_or_lose)
			{
				UserDefault::getInstance()->setIntegerForKey("total_select_chapter",m_nTotalSelectChapter-1);
				UserDefault::getInstance()->flush();
			}
			
			nextChapter(this);
			break;
		}
	case enGameOverMenu_Next:
		{
			nextChapter(this);
			break;
		}
	case enGameOverMenu_Facebook:
		{
			AdsManager::getInstance()->OpenURL("https://www.facebook.com/sharer/sharer.php?u=https://play.google.com/store/apps/details?id=com.magez.passitthrough");
			break;
		}
	case enGameOverMenu_Twitter:
		{
			//int level=UserDefault::getInstance()->getIntegerForKey("total_complete_chapter",0);
			//int score=UserDefault::getInstance()->getIntegerForKey("score",0);
			//AdsManager::postTW(level,score);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID ) || (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM != CC_PLATFORM_MAC)
            sdkbox::SocialShareInfo info;
            info.text = "Can you pass it through? Download here";
            info.title = "Pass It Through";
            info.image = this->_captureFilename;
            info.link = STORE_URL;
            info.platform = sdkbox::SocialPlatform::Platform_Twitter;
            info.showDialog = true;
            sdkbox::PluginShare::share(info);
#endif
			break;
		}
	case enGameOverMenu_Google:
		{
			break;
		}
	case enClose:
        {
            unlock_background->removeFromParent();
            showResult();
            break;
        }
    case enOk:
        {
            AdsManager::getInstance()->buyItem(UNLOCK_ALL_LEVELS);
            unlock_background->removeFromParent();
            break;
        }
	}
}
