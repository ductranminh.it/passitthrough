/************************************************************************
*  AboutScene.cpp
*
*
*  Created by udinsoft
*  Copyright udinsoft 2019-5-15
*
************************************************************************/
#include "AboutScene.h"
//#include "MainGameScene.h"
#include "FirstScene.h"



#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>

#endif

using namespace cocos2d;
using namespace CocosDenshion;

enum enAboutSceneMenu{
	enClose             = 1,
	enMenu
};

Scene* AboutScene::createScene(){
	return AboutScene::create();
}

// on "init" you need to initialize your instance
bool AboutScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!Scene::init()){
		return false;
	}

	initBackground();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(AboutScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(AboutScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(AboutScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	return true;
}


void AboutScene::initBackground(){
	setting_background = Sprite::create("about/broad.png");
 	setting_background->setPosition( Vec2(winSize.width *0.5f, winSize.height * 0.5f));
	CommonUtiles::setAdjustScale(setting_background);
	setting_background->setScale(setting_background->getScale()*0.3f);
	addChild(setting_background,2);
	float m_nWidth=setting_background->getContentSize().width;
	float m_nHeight=setting_background->getContentSize().height;

	auto label1 = Label::createWithTTF("Pass it through",fontList[3].c_str(),80,Size(m_nWidth, m_nHeight* 0.35f), TextHAlignment::CENTER);
	label1->setVerticalAlignment(TextVAlignment::CENTER);
	label1->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.8f));
	label1->setColor(Color3B(254, 201, 33));
	setting_background->addChild(label1, 1);

	auto label2 = Label::createWithTTF("2019 Magez Studio, All Rights Reserved", fontList[3].c_str(), 60, Size(m_nWidth, m_nHeight* 0.35f), TextHAlignment::CENTER);
	label2->setVerticalAlignment(TextVAlignment::CENTER);
	label2->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.7f));
	label2->setColor(Color3B(254, 255, 255));
	setting_background->addChild(label2, 1);

	auto label3 = Label::createWithTTF("Credits:", fontList[3].c_str(), 50, Size(m_nWidth, m_nHeight* 0.35f), TextHAlignment::CENTER);
	label3->setVerticalAlignment(TextVAlignment::CENTER);
	label3->setPosition(Vec2(m_nWidth*0.2f, m_nHeight*0.6f));
	label3->setColor(Color3B(254, 255, 255));
	setting_background->addChild(label3, 1);

	auto label4 = Label::createWithTTF("- Game engine :\n Powered by Cocos2d-x under MIT license", fontList[3].c_str(), 50, Size(m_nWidth, m_nHeight* 0.35f), TextHAlignment::CENTER);
	label4->setVerticalAlignment(TextVAlignment::CENTER);
	label4->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.53f));
	label4->setColor(Color3B(254, 255, 255));
	setting_background->addChild(label4, 1);

	auto label5 = Label::createWithTTF("- Background music :\n Antonyme Romeo - Variation #1", fontList[3].c_str(), 50, Size(m_nWidth, m_nHeight* 0.35f), TextHAlignment::CENTER);
	label5->setVerticalAlignment(TextVAlignment::CENTER);
	label5->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.4f));
	label5->setColor(Color3B(254, 255, 255));
	setting_background->addChild(label5, 1);

	//cancel
	MenuItemImage* btn_cancel = MenuItemImage::create("option_screen/close.png", "option_screen/close.png", CC_CALLBACK_1(AboutScene::backCallBack, this));
	btn_cancel->setTag(enClose);
	btn_cancel->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.05f));
	
	m_pMainMenu=Menu::create(btn_cancel,NULL);	
	m_pMainMenu->setPosition(Vec2::ZERO);
	m_pMainMenu->setTag(enMenu);
	setting_background->addChild(m_pMainMenu,3);

	Size vsize = Director::getInstance()->getVisibleSize();
	String* str_utf8 = String::createWithContentsOfFile("about/about.html");
	char buf[512];
	str_utf8->createWithFormat("%c",buf);
	////CCLog(buf);

	touch_status=false;
	instruction_state=false;
}

void AboutScene::backCallBack(Ref* pSender){
	switch(((MenuItem *)pSender)->getTag())
	{
	case enClose:			
		{
			this->getParent()->resume();

			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();
			//this->getParent()->removeChild(setting_background);

			break;
		}

	}
}

bool AboutScene::onTouchBegan(Touch* touch, Event* event){
	touchStart = setting_background->convertTouchToNodeSpace(touch);
	return false;
}

void AboutScene::onTouchMoved(Touch* touch, Event* event){
	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

void AboutScene::onTouchEnded(Touch* touch, Event* event)
{	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
	
}

