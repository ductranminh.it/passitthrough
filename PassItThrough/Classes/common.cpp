/************************************************************************
*  common.cpp															
*																		
*																		
*  Created by Lin dao min									
*  Copyright Lin 2014-4-5											
*																		
************************************************************************/


#include "common.h"

using namespace cocos2d;
using namespace CocosDenshion;

float fScaleN;
float fScaleX;
float fScaleY;
Size winSize;
Size winSizeInPixel;
float fScaleRetina;

std::string fontList[] =
{
#if( CC_TARGET_PLATFORM == CC_PLATFORM_IOS )
	"bauhaus 93.ttf",
	"bernhard std bold condensed.ttf",
	"mercurius ct std light.ttf",
	"Forte.ttf",
#else
	"fonts/bauhaus 93.ttf",
	"fonts/bernhard std bold condensed.ttf",
	"fonts/mercurius ct std light.ttf",
	"fonts/Forte.ttf",
#endif
};

//int gPlayingType = 0;		// 0: puzzle , 1 : arcade
int gLevelPageNum = 0;

//bool music;
//bool sound_effect;
bool share_facebook;

int m_nMainSceneAppearNumber=1;

bool first_install;
bool nothanks_flag;
bool first_booting=true;
bool rate_booting = true;
bool remove_ad = true;
bool ad_booting = true;
bool unlock_all = true;
bool is_iap_enabled = true;

char* page_index_string[4]={"1/4","2/4","3/4","4/4"};

int curve_num[LEVEL_KIND][CHAPTER_MAX]={{3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23},
									{3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23},
									{3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}

									};
int curve_difficulty[CHAPTER_MAX]={5,7,10,12,15,15,18,18,20,20,25,25,27,27,30,30,33,33,36,36};
//int selected_level=1;
//int selected_chapter=1;
bool level_or_chapter=false;
bool menu_halt=false;

float sound_volume=0.f;
float music_volume=0.f;

bool music_state=true;
bool sound_state=true;

bool sound_state_changed=false;
bool music_state_changed=false;

int high_score[5]={0,0,0,0,0};
std::string high_time[5]={"","","","",""};

//CCDictionary* strongHoldInfo;

void CommonUtiles::setNormalScale(Node* obj)
{
	obj->setScaleX(fScaleX * fScaleRetina);
	obj->setScaleY(fScaleY * fScaleRetina);
}

void CommonUtiles::setAdjustScale(Node* obj)
{
	obj->setScale(fScaleN * fScaleRetina);
}

