/************************************************************************
*  RateScene.cpp
*
*
*  Created by udinsoft
*  Copyright udinsoft 2019-5-15
*
************************************************************************/
#include "RateScene.h"
//#include "SelectLevelScene.h"

#include "MainGameScene.h"
#include "adsManager.h"
//#include "GameContinueScene.h"
#include "FirstScene.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>

#endif

using namespace cocos2d;
using namespace CocosDenshion;

enum enRateSceneMenu{
	
	enNow             = 1,
	enLater           = 2,
	enNo              = 3,
	enMenu
};

Scene* RateScene::createScene(){
	return RateScene::create();
}

// on "init" you need to initialize your instance
bool RateScene::init(){
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	initBackground();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(RateScene::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(RateScene::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(RateScene::onTouchMoved, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	return true;
}

void RateScene::initBackground(){

	//background
	setting_background = Sprite::create("rate/broad.png");
	CommonUtiles::setAdjustScale(setting_background);
 	setting_background->setPosition( Vec2(winSize.width *0.5f, winSize.height * 0.5f));
	setting_background->setScale(setting_background->getScale()*0.3f);
	addChild(setting_background,2);
	float m_nWidth=setting_background->getContentSize().width;
	float m_nHeight=setting_background->getContentSize().height;
	
	auto lb_chapter = Label::createWithTTF("Would you mind taking a moment to rate this game? It won't take more than a minute. Thanks for your support!", fontList[3], 60, Size(m_nWidth * 0.8f, m_nHeight * 0.5f), TextHAlignment::CENTER);
	lb_chapter->setVerticalAlignment(TextVAlignment::CENTER);
	lb_chapter->setPosition( Vec2(m_nWidth*0.5f, m_nHeight*0.6f));
	lb_chapter->setColor(Color3B(254,201,33));
	setting_background->addChild(lb_chapter,3);

	//cancel
	MenuItemImage* btn_now = MenuItemImage::create("rate/rate-it.png", "rate/rate-it.png", CC_CALLBACK_1(RateScene::backCallBack, this));
	btn_now->setTag(enNow);
	btn_now->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.4f));

	MenuItemImage* btn_later = MenuItemImage::create("rate/remind.png", "rate/remind.png", CC_CALLBACK_1(RateScene::backCallBack, this));
	btn_later->setTag(enLater);
	btn_later->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.3f));

	MenuItemImage* btn_no = MenuItemImage::create("rate/no.png", "rate/no.png", CC_CALLBACK_1(RateScene::backCallBack, this));
	btn_no->setTag(enNo);
	btn_no->setPosition(Vec2(m_nWidth*0.5f, m_nHeight*0.2f));
	
	m_pMainMenu=Menu::create(btn_now,btn_later,btn_no,NULL);	
	m_pMainMenu->setPosition(Vec2::ZERO);
	m_pMainMenu->setTag(enMenu);
	setting_background->addChild(m_pMainMenu,3);

	touch_status=false;
	instruction_state=false;
}

void RateScene::backCallBack(Ref* pSender){
	
	switch(((MenuItem *)pSender)->getTag())
	{
	
	case enNow:			
		{

			AdsManager::getInstance()->showRate();
			
			this->getParent()->resume();

			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();

			break;
		}
	case enLater:			
		{
			this->getParent()->resume();

			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();

			break;
		}
	case enNo:			
		{
			UserDefault::getInstance()->setBoolForKey("nothanks_flag",true);
			UserDefault::getInstance()->flush();

			this->getParent()->resume();

			menu_halt=false;
			this->removeFromParent();
			Director::getInstance()->resume();
			//this->getParent()->removeChild(setting_background);

			break;
		}

	}
}

bool RateScene::onTouchBegan(Touch* touch, Event* event){

	touchStart = setting_background->convertTouchToNodeSpace(touch);
	return false;
}

void RateScene::onTouchMoved(Touch* touch, Event* event){

	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

void RateScene::onTouchEnded(Touch* touch, Event* event){

	Vec2 curPos = setting_background->convertTouchToNodeSpace(touch);
}

